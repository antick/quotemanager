using System.Reflection;
using System.Runtime.CompilerServices;


[assembly: AssemblyCompany("Antick")]
[assembly: AssemblyProduct("Antick.IncApp.QuoteManager")]
[assembly: AssemblyCopyright("Copyright � Antick 2017")]
[assembly: AssemblyTrademark("Antick.IncApp.QuoteManager")]
[assembly: AssemblyCulture("")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]