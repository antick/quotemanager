﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Infractructure.Components.IO;
using Antick.Infractructure.Extensions;
using RestSharp;

namespace Antick.QuoteManager.Client
{
    public class QuoteManagerClient : IQuoteManagerClient
    {
        private readonly IListPackage<Quote> m_Package;
        private readonly string m_Address;
        private readonly bool m_Mock;

        public QuoteManagerClient(IListPackage<Quote> package, string address, bool mock)
        {
            m_Package = package;
            m_Address = address;
            m_Mock = mock;
        }

        public async Task<DayResponse> GetQuotes(string instrument, DateTime day)
        {
            if (m_Mock)
            {
                return await Task.FromResult(new DayResponse());
            }

            var dayEncoded = day.ToUtcUrlEncode();

            var client = new RestClient(m_Address);
            var request = new RestRequest("api/quote/getDayBinary?instrument={instrument}&day={day}", Method.GET);
            request.AddUrlSegment("instrument", instrument);
            request.AddUrlSegment("day", dayEncoded);

            var response = client.Execute<DayBinaryResponse>(request);

            if (response == null || response.Data.BinaryData == string.Empty)
                return new DayResponse { Candles = null, LastCompletedDay = response?.Data.LastCompletedDay };

            var data = m_Package.ReadFromBase64(response.Data.BinaryData, PackageMode.PackedZip);

            return new DayResponse { Candles = data, LastCompletedDay = response.Data.LastCompletedDay };
        }

        public async Task<DayResponse> GetQuotesFrom(string instrument, DateTime time)
        {
            if (m_Mock)
            {
                return await Task.FromResult(new DayResponse());
            }

            var timeEncoded = time.ToUtcUrlEncode();

            var client = new RestClient(m_Address);
            var request = new RestRequest("api/quote/getDayBinaryFrom?instrument={instrument}&from={time}", Method.GET);
            request.AddUrlSegment("instrument", instrument);
            request.AddUrlSegment("time", timeEncoded);

            var response = client.Execute<DayBinaryResponse>(request);

            if (response == null || response.Data.BinaryData == string.Empty)
                return new DayResponse { Candles = null, LastCompletedDay = response?.Data.LastCompletedDay };

            var data = m_Package.ReadFromBase64(response.Data.BinaryData, PackageMode.PackedZip);

            return new DayResponse { Candles = data, LastCompletedDay = response.Data.LastCompletedDay };
        }

        public async Task<Quote> GetFirstQuote(string instrument)
        {
            var client = new RestClient(m_Address);
            var request = new RestRequest("api/quote/getFirst?instrument={instrument}", Method.GET);
            request.AddUrlSegment("instrument", instrument); // replaces matching token in request.Resource

            var response = client.Execute<Quote>(request);
            return response.Data;
        }
    }
}
