﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Antick.QuoteManager.Client")]
[assembly: AssemblyDescription("Antick.QuoteManager api client")]
[assembly: AssemblyCompany("Antick")]
[assembly: AssemblyProduct("Antick.QuoteManager.Client")]
[assembly: AssemblyCopyright("Copyright © Antick 2017")]
[assembly: AssemblyTrademark("Antick.QuoteManager.Client")]
[assembly: AssemblyCulture("")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("509001aa-972c-45fa-95ce-908454ba0e47")]


