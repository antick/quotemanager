﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;

namespace Antick.QuoteManager.Client
{
    public interface IQuoteManagerClient
    {
        Task<DayResponse> GetQuotes(string instrument, DateTime day);
        Task<DayResponse> GetQuotesFrom(string instrument, DateTime time);
        Task<Quote> GetFirstQuote(string instrument);
    }
}
