﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.Filters;
using Antick.ApiHosting;
using Antick.ApiHosting.Windsor;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs;
using Antick.Db;
using Antick.Infractructure.Components;
using Antick.Infractructure.Components.Agent;
using Antick.Infractructure.Components.IO;
using Antick.Infractructure.Components.Misc;
using Antick.Messaging.Bus;
using Antick.OandaApi;
using Antick.QuoteManager.Components.Agents;
using Antick.QuoteManager.Components.FileCache;
using Antick.QuoteManager.Components.MemoryCache;
using Antick.QuoteManager.Cqs.Commands;
using Antick.QuoteManager.Cqs.Commands.Context;
using Antick.QuoteManager.Db;
using Antick.QuoteManager.WebApi.Configuration;
using Castle.Facilities.Quartz;
using Castle.Facilities.Startable;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Inceptum.AppServer.Configuration;

namespace Antick.QuoteManager.Windsor
{
    public class BackendInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            installCommonFacilities(container);

            container.Install(new BusInstaller("transports", "EasyNetQ"));

            container.Install(
                new DefaultApiHostingInstaller<MainApiHostConfigurator>(Types.FromAssemblyInThisApplication(),
                    "infractructure", "ApiHost", "Network"));

            container.Register(Component.For<LoggingInterceptor>());

            container.Install(new SessionBuilderWindsorInstaller<QuoteContext>("main"));
            container.Install(new CqrsInstaller(Types.FromAssemblyInThisApplication()));

            configureComponents(container);
        }

        private void installCommonFacilities(IWindsorContainer container)
        {
            // Common facilities
            container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel));
            container.AddFacility<StartableFacility>(f => f.DeferredStart());
            container.AddFacility<TypedFactoryFacility>();
            container.AddFacility<QuartzFacility>();

            // Config
            container.AddFacility<ConfigurationFacility>(f => f.ConfigureConnectionStrings("transports", "{environment}", "{machineName}"));
        }

        private void configureComponents(IWindsorContainer container)
        {
            container.Register(Component.For<IOandaClientV20>()
                .ImplementedBy<OandaClientV20>()
                .LifestyleSingleton()
                .DependsOnBundle("components", "OandaClient", "{environment}", "{machineName}"));

            container.Register(Component.For<IAgent>().ImplementedBy<LoadAgent>().LifestyleSingleton()
                .Named("LoadAgent"));
            container.Register(Component.For<LoadAgentConfiguration>()
                .FromConfiguration("components", "CandleSyncAgent", "{environment}", "{machineName}"));
            container.Register(Component.For<IAgent>().ImplementedBy<HistoryCacheAgent>().LifestyleSingleton()
                .Named("HitoryCacheAgent"));


            container.Register(Component.For<IHistoryCache>().ImplementedBy<HistoryCache>()
                .DependsOnBundle("components", "HistoryCache", "{environment}", "{machineName}"));

            container.Register(Component.For<IClock>().ImplementedBy<Clock>());


            container.Register(Component.For<IQuotesMemoryCache, IMemoryCache>().ImplementedBy<QuotesMemoryCache>());
            container.Register(Component.For<IHistoryLoaderLastTimeMemoryCache, IMemoryCache>()
                .ImplementedBy<HistoryLoaderLastTimeMemoryCache>());



            container.Register(Component.For<IListPackage<Quote>>().ImplementedBy<ListPackage<Quote>>());
        }
    }
}
