﻿using System;

namespace Antick.QuoteManager.Domain
{
    public class HistoryLoaderStatus
    {
        public int Id { get; private set; }

        public string Instrument { get; private set; }
        
        public DateTime LastTime { get; private set; }

        public HistoryLoaderStatus(int id, string instrument, DateTime lastTime)
        {
            Id = id;
            Instrument = instrument;
            LastTime = lastTime;
        }

        public void UpdateLastTime(DateTime lastTime)
        {
            LastTime = lastTime;
        }
    }
}
