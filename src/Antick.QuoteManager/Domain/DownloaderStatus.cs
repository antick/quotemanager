﻿using System;

namespace Antick.QuoteManager.Domain
{
    public class DownloaderStatus
    {
        public int Id { get; private set; }

        public string Instrument { get; private set; }

        public DateTime StartTime { get; private set; }

        public int CountCandles { get; private set; }

        public DateTime Time { get; private set; }

        public DownloaderStatus(int id, string instrument, DateTime time, DateTime startTime, int countCandles)
        {
            Id = id;
            Instrument = instrument;
            Time = time;
            StartTime = startTime;
            CountCandles = countCandles;
        }
    }


}
