﻿using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.QuoteManager.Db.Dto;

namespace Antick.QuoteManager.Extensions
{
    public static class QuoteExtensions
    {
        public static QuoteDto ToDto(this Quote domain, string instrument)
        {
            return new QuoteDto
            {
                Instrument = instrument,
                Time = domain.Time,
                Open = domain.Open,
                Close = domain.Close,
                High = domain.High,
                Low = domain.Low,
                Index = domain.Index
            };
        }
    }
}
