﻿using Antick.QuoteManager.Db.Dto;
using Antick.QuoteManager.Domain;

namespace Antick.QuoteManager.Extensions
{
    public static class DownloaderStatusDtoExtensions
    {
        public static DownloaderStatus ToDomain(this DownloaderStatusDto dto)
        {
            return new DownloaderStatus(dto.Id, dto.Instrument, dto.Time, dto.StartTime, dto.CountCandles);
        }
    }
}
