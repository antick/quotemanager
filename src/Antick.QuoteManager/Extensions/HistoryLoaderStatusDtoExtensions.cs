﻿using Antick.QuoteManager.Db.Dto;
using Antick.QuoteManager.Domain;

namespace Antick.QuoteManager.Extensions
{
    public static class HistoryLoaderStatusDtoExtensions
    {
        public static HistoryLoaderStatus ToDomain(this HistoryLoaderStatusDto dto)
        {
            return new HistoryLoaderStatus(dto.Id,dto.Instrument,dto.LastDate);
        }
    }
}
