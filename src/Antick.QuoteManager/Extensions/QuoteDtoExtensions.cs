﻿using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.QuoteManager.Db.Dto;

namespace Antick.QuoteManager.Extensions
{
    public static class QuoteDtoExtensions
    {
        public static Quote ToContract(this QuoteDto dto)
        {
            return new Quote
            {
                Open = dto.Open,
                Close = dto.Close,
                High = dto.High,
                Low = dto.Low,
                Time = dto.Time,
                Index = dto.Index,
            };
        }
    }
}
