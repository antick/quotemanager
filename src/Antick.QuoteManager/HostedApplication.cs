﻿using System;
using System.Threading.Tasks;
using Antick.ApiHosting.WebApi;
using Antick.ApiHosting.WebApi.Configuration;
using Antick.Cqrs.Commands;
using Antick.Infractructure.Components.Agent;
using Antick.QuoteManager.Components.MemoryCache;
using Antick.QuoteManager.Cqs.Commands.Context;
using Castle.Core.Logging;
using Inceptum.AppServer;

namespace Antick.QuoteManager
{
    internal class HostedApplication : IHostedApplication
    {
        private readonly ICommandBuilder m_CommandBuilder;
        private readonly ILogger m_Logger;
        private readonly ApiHost m_ApiHost;
        private readonly NetworkConfig m_NetworkConfig;
        private readonly IAgent[] m_BackAgents;
        private readonly IMemoryCache[] m_MemoryCaches;

        public HostedApplication(ICommandBuilder commandBuilder, ILogger logger, ApiHost apiHost, NetworkConfig networkConfig,
            IAgent[] backAgents, IMemoryCache[] memoryCaches)
        {
            m_CommandBuilder = commandBuilder;
            m_Logger = logger;
            m_ApiHost = apiHost;
            m_NetworkConfig = networkConfig;
            m_BackAgents = backAgents;
            m_MemoryCaches = memoryCaches;
        }

        public void Start()
        {
            m_Logger.InfoFormat("Starting application...");

            AppDomain.CurrentDomain.UnhandledException += onUnhandledException;
            TaskScheduler.UnobservedTaskException +=
                (sender, args) => m_Logger.Error("Unobserved task exception", args.Exception);

            m_Logger.DebugFormat("Configuring web api hosts...");
            m_NetworkConfig.SetupNetwork();
            m_ApiHost.Start();
            m_Logger.InfoFormat("Web api host Configured");

            foreach (var memoryCache in m_MemoryCaches)
            {
                m_Logger.DebugFormat("Starting {0}", memoryCache.GetType().Name);
                memoryCache.Init();
            }

            foreach (var agent in m_BackAgents)
            {
                m_Logger.DebugFormat("Starting {0}", agent.GetType().Name);
                agent.Start();
            }


            m_Logger.InfoFormat("Application started");
        }


        void onUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = e.ExceptionObject as Exception;
            if (ex != null)
            {
                m_Logger.ErrorFormat(ex, "Unhandled exception occured");
            }
            else
            {
                m_Logger.Error("Unhandled exception occured. No extra information can be provided.");
            }
        }
    }
}
