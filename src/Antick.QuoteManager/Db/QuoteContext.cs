using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Antick.QuoteManager.Db.Dto;
using Castle.Core.Logging;
using Inceptum.AppServer.Configuration;

namespace Antick.QuoteManager.Db
{
    public partial class QuoteContext : DbContext
    {
        static QuoteContext()
        {
            Database.SetInitializer<DbContext>(null); // Database initialization is not required
        }

        public QuoteContext(ConnectionString connectionString, ILogger logger)
            : base(connectionString)
        {
        }

        public virtual DbSet<QuoteDto> Quotes { get; set; }

        public virtual DbSet<HistoryLoaderStatusDto> QuoteFileLoaderStatus { get; set; }

        public virtual DbSet<DownloaderStatusDto> Downloder { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<QuoteDto>()
                .Property(e => e.Instrument)
                .IsUnicode(false);

            modelBuilder.Entity<HistoryLoaderStatusDto>()
                .Property(e => e.Instrument)
                .IsUnicode(false);

        }
    }
}
