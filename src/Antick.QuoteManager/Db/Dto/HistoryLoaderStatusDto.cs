﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Antick.QuoteManager.Db.Dto
{
    [Table("QuoteFileLoaderStatus")]
    public class HistoryLoaderStatusDto
    {
        public int Id { get; set; }

        public string Instrument { get; set; }

        public DateTime LastDate { get; set; }
    }
}
