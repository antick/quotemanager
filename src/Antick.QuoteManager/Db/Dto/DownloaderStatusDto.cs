﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Antick.QuoteManager.Db.Dto
{
    [Table("DownloaderStatus")]
    public class DownloaderStatusDto
    {
        public int Id { get; set; }

        public string Instrument { get; set; }

        public DateTime StartTime { get; set; }

        public int CountCandles { get; set; }

        public DateTime Time { get; set; }
    }
}
