using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Antick.QuoteManager.Db.Dto
{
    [Table("Quote")]
    public partial class QuoteDto
    {
        public long Id { get; set; }

        public DateTime Time { get; set; }

        public double Open { get; set; }

        public double Close { get; set; }

        public double High { get; set; }

        public double Low { get; set; }

        public long Index { get; set; }

        [Required]
        [StringLength(50)]
        public string Instrument { get; set; }
    }
}
