﻿using System;

namespace Antick.QuoteManager.WebApi.Models
{
    public class GetDayBinaryResponse
    {
        /// <summary>
        /// Биннарные данные за указанный день
        /// </summary>
        public string BinaryData { get; set; }
        
        /// <summary>
        /// Последний завершенный день согластно кэшу
        /// </summary>
        public DateTime? LastCompletedDay { get; set; }
    }
}
