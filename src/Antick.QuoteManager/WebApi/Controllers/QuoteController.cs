﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Queries;
using Antick.QuoteManager.Cqs.Queries.Criterias;
using Antick.QuoteManager.WebApi.Models;
using Inceptum.WebApi.Help.Description;

namespace Antick.QuoteManager.WebApi.Controllers
{
    [RoutePrefix("api/quote")]
    public class QuoteController : ApiController
    {
        private readonly IQueryBuilder m_QueryBuilder;

        public QuoteController(IQueryBuilder queryBuilder)
        {
            m_QueryBuilder = queryBuilder;
        }
      

        /// <summary>
        /// Выгрузка первого бара
        /// </summary>
        /// <param name="instrument"></param>
        /// <returns></returns>
        [HttpGet, Route("getFirst")]
        [ResponseType(typeof(Quote))]
        public async Task<HttpResponseMessage> First([FromUri(Name = "instrument")] string instrument)
        {
            var Quote = m_QueryBuilder.For<Quote>().With(new GetFirstCandle {Instrument = instrument});
            return Request.CreateResponse(Quote);
        }

        /// <summary>
        /// Выгрузка тиковых котировок в бинарном виде, за 1 день.
        /// </summary>
        /// <param name="instrument">Инструмент по которому выгружаем</param>
        /// <param name="day">День, за который выгружаем</param>
        /// <returns>бинарные данные в формате Base64</returns>
        [HttpGet, Route("getDayBinary")]
        [ResponseType(typeof(GetDayBinaryResponse))]
        [ApiExplorerOrder(Order = 4)]
        public async Task<HttpResponseMessage> GetDayBinary(
            [FromUri(Name = "instrument")] string instrument,
            [FromUri(Name = "day")] string dayStr)
        {
            var day = DateTime.Parse(HttpUtility.UrlDecode(dayStr));

            var binaryData = m_QueryBuilder.For<string>()
                .With(new GetBinaryCandlesInDay {Instrument = instrument, Day = day});

            var lastDate = m_QueryBuilder.For<DateTime?>()
                .With(new GetHistoryLoaderLastTime {Instrument = instrument, UseCache = true});

            var response = new GetDayBinaryResponse {BinaryData = binaryData, LastCompletedDay = lastDate};

            var resp = Request.CreateResponse(response);
            resp.Content.Headers.Add("Content-Logging","false");

            return await Task.FromResult(resp);
        }

        /// <summary>
        /// Выгрузка тиковых котировок в бинарном виде за актуальный не законченный день. Выгружает котировки с указанного промежутка(не включает его)
        /// </summary>
        /// <param name="instrument">Инструмент, по котором выгружаем</param>
        /// <param name="from">Время начала выгрузки(не включается)</param>
        /// <returns>бинарные данные в формате Base64</returns>
        [HttpGet, Route("getDayBinaryFrom")]
        [ResponseType(typeof(string))]
        [ApiExplorerOrder(Order = 4)]
        public async Task<HttpResponseMessage> GetDayBinaryFrom(
            [FromUri(Name = "instrument")] string instrument,
            [FromUri(Name = "from")] string fromStr)
        {
            var from = DateTime.Parse(HttpUtility.UrlDecode(fromStr));

            var binaryData = m_QueryBuilder.For<string>().With(new GetBinaryCandlesFrom{Instrument = instrument, From = from});
            var lastDate = m_QueryBuilder.For<DateTime?>()
                .With(new GetHistoryLoaderLastTime { Instrument = instrument, UseCache = true });

            var response = new GetDayBinaryResponse { BinaryData = binaryData, LastCompletedDay = lastDate };

            var resp = Request.CreateResponse(response);
            resp.Content.Headers.Add("Content-Logging", "false");

            return resp;
        }




    }
}
