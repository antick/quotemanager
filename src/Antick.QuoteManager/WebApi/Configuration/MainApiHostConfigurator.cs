﻿using System;
using System.IO;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.Filters;
using System.Web.Http.SelfHost;
using Antick.ApiHosting.WebApi.Configuration;
using Antick.ApiHosting.WebApi.Routing;
using Castle.Core.Logging;
using Inceptum.WebApi.Help;

namespace Antick.QuoteManager.WebApi.Configuration
{
    internal class MainApiHostConfigurator : ApiHostConfiguratorDefault
    {
        public MainApiHostConfigurator(ILogger logger, ApiHostConfiguration configuration, IHttpControllerActivator httpControllerActivator, IFilter[] filters)
            : base(logger, configuration, httpControllerActivator, filters)
        {
        }

        protected override void OnConfigure(HttpSelfHostConfiguration configuration)
        {
            base.OnConfigure(configuration);
            configureRoutes(configuration);
            configureHelpPage(configuration);
        }

        private static void configureRoutes(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            configureInfrastructureRoutes(config);
        }

        private static void configureInfrastructureRoutes(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute("AppStatus",
                routeTemplate: "v{version}/appStatus",
                constraints: RouteConstraints.AnyVersionHttpGet,
                defaults: new { controller = "Config", action = "GetAppStatus", version = "1" });
        }

        private void configureHelpPage(HttpConfiguration config)
        {
            if (config == null) throw new ArgumentNullException("config");

            config.UseHelpPage(cfg =>
            {
                if (Configuration.HelpPageSamplesUri != null)
                {
                    cfg.SamplesUri(Configuration.HelpPageSamplesUri);
                }
                cfg.WithDocumentationProvider(new XmlDocumentationProvider(Path.Combine(Environment.CurrentDirectory, "content")));
            });
        }
    }
}
