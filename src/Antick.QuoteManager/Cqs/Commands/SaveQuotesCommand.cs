﻿using System.Linq;
using Antick.Contracts.Msgs.QuotesApi;
using Antick.Cqrs.Commands;
using Antick.Db;
using Antick.Infractructure.Components;
using Antick.Infractructure.Components.Misc;
using Antick.QuoteManager.Components.MemoryCache;
using Antick.QuoteManager.Cqs.Commands.Context;
using Antick.QuoteManager.Db;
using Antick.QuoteManager.Extensions;
using Castle.Core.Logging;
using EasyNetQ;

namespace Antick.QuoteManager.Cqs.Commands
{
    public class SaveQuotesCommand : ICommand<SaveQuotesCommandContext>
    {
        private readonly ISessionBuilder<QuoteContext> m_SessionBuilder;
        private readonly ICommandBuilder m_CommandBuilder;
        private readonly ILogger m_Logger;
        private readonly IQuotesMemoryCache m_MemoryCache;
        private readonly IBus m_Bus;
        private readonly IClock m_Clock;

        public SaveQuotesCommand(ISessionBuilder<QuoteContext> sessionBuilder, ICommandBuilder commandBuilder, ILogger logger, IQuotesMemoryCache memoryCache, IBus bus, IClock clock)

        {
            m_SessionBuilder = sessionBuilder;
            m_CommandBuilder = commandBuilder;
            m_Logger = logger;
            m_MemoryCache = memoryCache;
            m_Bus = bus;
            m_Clock = clock;
        }

        public void Execute(SaveQuotesCommandContext commandContext)
        {
            var lastQuote = m_MemoryCache.GetLast(commandContext.Instrument);
            long index = 0;
            if (lastQuote != null)
                index = lastQuote.Index;

            foreach (var quote in commandContext.Quotes)
            {
                quote.Index = index + 1;
                index = quote.Index;
            }

            // Запись в БД
            m_SessionBuilder.Execute(session =>
            {
                var dtos = commandContext.Quotes.Select(x => x.ToDto(commandContext.Instrument)).ToList();
                session.BulkInsertAsync(dtos).Wait();
                session.Save();
            });

            // Обновление кэша в памяти
            m_MemoryCache.Update(commandContext.Instrument,commandContext.Quotes);
            
            // Публикация эвента о новых котировках
            m_Bus.Publish(new NewQuotes
            {
                Instrument = commandContext.Instrument,
                Quotes = commandContext.Quotes,
                CreatedTime = m_Clock.UtcNow()
            });

            m_Logger.InfoFormat("Instrument {0} saved {1} quotes",commandContext.Instrument,commandContext.Quotes.Count);
        }
    }
}
