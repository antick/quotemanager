﻿using System;
using Antick.Cqrs.Commands;

namespace Antick.QuoteManager.Cqs.Commands.Context
{
    public class UpdateDownloderStatusCommandContext : ICommandContext
    {
        public string Instrument { get; set; }

        public DateTime StartTime { get; set; }

        public int CountCandles { get; set; }

        public DateTime Time { get; set; }
    }
}
