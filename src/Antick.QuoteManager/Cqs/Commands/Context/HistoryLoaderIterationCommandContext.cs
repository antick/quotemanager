﻿using Antick.Cqrs.Commands;

namespace Antick.QuoteManager.Cqs.Commands.Context
{
    public class HistoryLoaderIterationCommandContext : ICommandContext
    {
        public bool UseAllInstruments { get; set; }

        public string Instrument { get; set; }
    }
}
