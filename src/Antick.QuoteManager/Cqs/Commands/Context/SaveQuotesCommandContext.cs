﻿using System.Collections.Generic;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Commands;

namespace Antick.QuoteManager.Cqs.Commands.Context
{
    public class SaveQuotesCommandContext : ICommandContext
    {
        public List<Quote> Quotes { get; set; }

        public string Instrument { get; set; }
    }
}
