﻿using System;
using Antick.Cqrs.Commands;

namespace Antick.QuoteManager.Cqs.Commands.Context
{
    public class SaveHistoryLoaderStatusCommandContext : ICommandContext
    {
        public string Instrument { get; set; }

        public DateTime LastTime { get; set; }
    }
}
