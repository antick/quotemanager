﻿using System;
using System.Collections.Generic;
using System.Linq;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.Infractructure.Components;
using Antick.Infractructure.Components.Misc;
using Antick.Infractructure.Extensions;
using Antick.QuoteManager.Components.FileCache;
using Antick.QuoteManager.Cqs.Commands.Context;
using Antick.QuoteManager.Cqs.Queries.Criterias;
using Antick.QuoteManager.Domain;
using Castle.Core.Logging;

namespace Antick.QuoteManager.Cqs.Commands
{
    public class HistoryLoaderIterationCommand : ICommand<HistoryLoaderIterationCommandContext>
    {
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly IClock m_Clock;
        private readonly ILogger m_Logger;
        private readonly ICommandBuilder m_CommandBuilder;
        private readonly IHistoryCache m_HistoryCache;

        public HistoryLoaderIterationCommand(IQueryBuilder queryBuilder, IClock clock, ILogger logger, ICommandBuilder commandBuilder, IHistoryCache historyCache)
        {
            m_QueryBuilder = queryBuilder;
            m_Clock = clock;
            m_Logger = logger;
            m_CommandBuilder = commandBuilder;
            m_HistoryCache = historyCache;
        }

        public void Execute(HistoryLoaderIterationCommandContext commandContext)
        {
            var instruments = commandContext.UseAllInstruments
                ? m_QueryBuilder.For<string[]>().With<GetWorkingInstruments>()
                : new[] { commandContext.Instrument };
            
            while (true)
            {
                List<InstrumentResult> result = instruments.Select(iterate).ToList();

                // при любой записи любого инструмента
                if (result.Any(p => p.Changed()))
                {
                }

                // Выход только когда по всем 
                if (result.All(p => p.Completed()))
                {
                    m_Logger.DebugFormat("Cycle is completed.");
                    break;
                }
                // Если хотя бы по одному инструменту не дошли до текущего дня значит цикл не закончен
                else
                {
                    m_Logger.DebugFormat("Cycle is not completed, will continue");
                }
            }
        }


        private InstrumentResult iterate(string instrument)
        {
            var nextDay =
                m_QueryBuilder.For<DateTime?>().With(new GetNextDateForHistoryLoader {Instrument = instrument});

            // Нет данных ( вобще нет )
            if (nextDay == null)
            {
                return new InstrumentResult {NoData = true};
            }

            var day = nextDay.Value;
            
            var isCurrentDay = m_QueryBuilder.For<bool>().With(new IsTimeInCurrentDay {Time = day});
            if (isCurrentDay)
            {
                m_Logger.DebugFormat("Instrument {0} current day {1} is not completed", instrument, day.ToString("yyyy/MM/dd"));
                return new InstrumentResult {IsCurrentDay = true};
            }

            var downloaderStatus = m_QueryBuilder.For<DownloaderStatus>()
                .With(new GetDownloaderStatus {Instrument = instrument});
            
            var completedDay = false;
            if (downloaderStatus != null)
            {
                var now = m_Clock.UtcNow();
                // Выкачали допустим в пятницу, последнее время проверки суббота, новых данных нет, и сегодня суббота или воскресенье
                // значит пятница загружена полностью
                completedDay = (downloaderStatus.Time - downloaderStatus.StartTime).TotalDays > 0 &&
                                   downloaderStatus.Time.Day != downloaderStatus.StartTime.Day &&
                                   downloaderStatus.CountCandles == 0 &&
                                   (now.DayOfWeek == DayOfWeek.Saturday || now.DayOfWeek == DayOfWeek.Sunday);
            }

            var isLastDay = m_QueryBuilder.For<bool>().With(new IsTimeInLastDay {Instrument = instrument, Time = day});
            if (isLastDay && !completedDay)
            {
                m_Logger.DebugFormat("Instrument {0} day {1} is not completed", instrument, day.ToString("yyyy/MM/dd"));
                return new InstrumentResult { IsLastDay = true };
            }

            // Выкачка
            var quotes = m_QueryBuilder.For<List<Quote>>()
                .With(new GetQuotesInDay
                {
                    Instrument = instrument,
                    Day = day
                });

            if (!quotes.Any())
            {
                m_Logger.DebugFormat("Intstrument {0} no quotes for day {1}", instrument, day.ToString("yyyy/MM/dd"));
                m_CommandBuilder.Execute(new SaveHistoryLoaderStatusCommandContext { Instrument = instrument, LastTime = day });
                return new InstrumentResult {NoQuotesInCurrentDay = true};
            }

            // Сохранение на диск
            m_HistoryCache.Save(quotes, instrument, day).GetSynchronousResult();

            m_Logger.DebugFormat("Intstrument {0} saved for day {1}", instrument, day.ToString("yyyy/MM/dd"));

            m_CommandBuilder.Execute(new SaveHistoryLoaderStatusCommandContext { Instrument = instrument, LastTime = day });

            return new InstrumentResult();
        }

        /// <summary>
        /// Результат обхода одного инструмента
        /// </summary>
        private class InstrumentResult
        {
            public bool NoData { get; set; }

            public bool IsCurrentDay { get; set; }

            public bool IsLastDay { get; set; }

            public bool NoQuotesInCurrentDay { get; set; }

            /// <summary>
            /// Были изменения по инстременту
            /// </summary>
            /// <returns></returns>
            public bool Changed()
            {
                return NoQuotesInCurrentDay = false;
            }

            /// <summary>
            /// Завершен цикл (дошли до текущего дня)
            /// </summary>
            /// <returns></returns>
            public bool Completed()
            {
                return IsCurrentDay || IsLastDay || NoData;
            }
        }
    }
}
