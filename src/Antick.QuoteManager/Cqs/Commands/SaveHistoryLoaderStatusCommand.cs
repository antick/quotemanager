﻿using System.Linq;
using Antick.Cqrs.Commands;
using Antick.Db;
using Antick.QuoteManager.Components.MemoryCache;
using Antick.QuoteManager.Cqs.Commands.Context;
using Antick.QuoteManager.Db;
using Antick.QuoteManager.Db.Dto;
using Castle.Core.Logging;

namespace Antick.QuoteManager.Cqs.Commands
{
    public class SaveHistoryLoaderStatusCommand : ICommand<SaveHistoryLoaderStatusCommandContext>
    {
        private readonly ISessionBuilder<QuoteContext> m_SessionBuilder;
        private readonly ILogger m_Logger;
        private readonly IHistoryLoaderLastTimeMemoryCache m_MemoryCache;

        public SaveHistoryLoaderStatusCommand(ISessionBuilder<QuoteContext> sessionBuilder, ILogger logger, IHistoryLoaderLastTimeMemoryCache memoryCache)
        {
            m_SessionBuilder = sessionBuilder;
            m_Logger = logger;
            m_MemoryCache = memoryCache;
        }


        public void Execute(SaveHistoryLoaderStatusCommandContext commandContext)
        {
            // Сохранение в БД
            m_SessionBuilder.Execute(session =>
            {
                var dto = session.Query<HistoryLoaderStatusDto>()
                    .FirstOrDefault(p => p.Instrument == commandContext.Instrument);

                if (dto != null)
                    dto.LastDate = commandContext.LastTime;

                else
                {
                    session.Add(new HistoryLoaderStatusDto
                    {
                        Instrument = commandContext.Instrument,
                        LastDate = commandContext.LastTime
                    });
                }

                session.Save();
            });

            // Обновление кэша
            m_MemoryCache.Update(commandContext.Instrument,commandContext.LastTime);

            m_Logger.InfoFormat("Instrument {0} saved on {1}", commandContext.Instrument,
                commandContext.LastTime.ToString("u"));
        }
    }
}
