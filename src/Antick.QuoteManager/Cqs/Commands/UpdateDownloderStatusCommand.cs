﻿using System.Linq;
using Antick.Cqrs.Commands;
using Antick.Db;
using Antick.QuoteManager.Cqs.Commands.Context;
using Antick.QuoteManager.Db;
using Antick.QuoteManager.Db.Dto;

namespace Antick.QuoteManager.Cqs.Commands
{
    public class UpdateDownloderStatusCommand : ICommand<UpdateDownloderStatusCommandContext>
    {
        private readonly ISessionBuilder<QuoteContext> m_SessionBuilder;

        public UpdateDownloderStatusCommand(ISessionBuilder<QuoteContext> sessionBuilder)
        {
            m_SessionBuilder = sessionBuilder;
        }

        public void Execute(UpdateDownloderStatusCommandContext commandContext)
        {
            m_SessionBuilder.Execute(session =>
            {
                var prevDto = session.Query<DownloaderStatusDto>()
                    .FirstOrDefault(p => p.Instrument == commandContext.Instrument);

                if (prevDto == null)
                {
                    session.Add(new DownloaderStatusDto
                    {
                        Instrument = commandContext.Instrument,
                        Time = commandContext.Time,
                        StartTime = commandContext.StartTime,
                        CountCandles = commandContext.CountCandles
                    });
                }
                else
                {
                    prevDto.Time = commandContext.Time;
                    prevDto.StartTime = commandContext.StartTime;
                    prevDto.CountCandles = commandContext.CountCandles;
                }

                session.Save();
            });
        }
    }
}
