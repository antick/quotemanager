﻿using System;
using System.Collections.Generic;
using System.Linq;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Handlers;
using Antick.Cqrs.Queries;
using Antick.QuoteManager.Cqs.Commands.Context;
using Antick.QuoteManager.Cqs.Handlers.HandlerContext;
using Antick.QuoteManager.Cqs.Queries.Criterias;
using Castle.Core.Logging;

namespace Antick.QuoteManager.Cqs.Handlers
{
    public class LoadAgentHandler : IHandler<LoadAgentHandlerContext,TimeSpan>
    {
        private readonly ICommandBuilder m_CommandBuilder;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ILogger m_Logger;

        /// <summary>
        /// Кол-во баров, при превышении которорго считаем что нужно без задержки дергать АПИ (не догружены исторические котировки)
        /// </summary>
        private const int GRUB_COUNT = 5;

        public LoadAgentHandler(ICommandBuilder commandBuilder, IQueryBuilder queryBuilder, ILogger logger)
        {
            m_CommandBuilder = commandBuilder;
            m_QueryBuilder = queryBuilder;
            m_Logger = logger;
        }


        public TimeSpan Execute(LoadAgentHandlerContext criterion)
        {
            var grubMode = false;
            var anyDownloaded = false;

            foreach (var instrument in criterion.Instruments)
            {
                // выкачка данных
                int newQuotes = downloadAndSave(instrument,criterion.TimeFrame,criterion.DefaultStartTime);

                // подведение итогов
                if (newQuotes > 0)
                {
                    anyDownloaded = true;
                    grubMode |= newQuotes > GRUB_COUNT;
                }
            }

            // установка времени ожидания относительно текущего
            var delayTime = m_QueryBuilder.For<TimeSpan>()
                .With(new CalculateDelayTime
                {
                    IsGrubMode = grubMode,
                    AnyDownloaded = anyDownloaded,
                    TimeFrame = criterion.TimeFrame
                });

            return delayTime;
        }


        /// <summary>
        /// Выкачка и сохранение данныых
        /// </summary>
        private int downloadAndSave(string instrument, string timeFrame, DateTime defaultStartTime)
        {
            // Установка последнего времени котировки
            DateTime? startTime = m_QueryBuilder.For<DateTime?>()
                .With(new GetLastTime { Instrument = instrument });

            // Скачка новых данных (исключая время последней котировки )
            var quotes = m_QueryBuilder.For<List<Quote>>()
                .With(new DownloadQuotes
                {
                    Instrument = instrument,
                    TimeFrame = timeFrame,
                    StartTime = startTime,
                    DefaultStartTime = defaultStartTime
                });

            if (quotes.Count > 0)
            {
                m_CommandBuilder.Execute(new SaveQuotesCommandContext { Instrument = instrument, Quotes = quotes });

                m_Logger.DebugFormat("Instrument {0} saved {1} quotes, lastTime is {2}", instrument, quotes.Count, quotes.Last().Time.ToString("s"));
            }
            else
            {
                m_Logger.DebugFormat("Instrument {0} no new completed quotes", instrument);
            }

            return quotes.Count;
        }
    }
}
