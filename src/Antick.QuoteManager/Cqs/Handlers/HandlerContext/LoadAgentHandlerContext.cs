﻿using System;
using Antick.Cqrs.Handlers;

namespace Antick.QuoteManager.Cqs.Handlers.HandlerContext
{
    public class LoadAgentHandlerContext : IHandlerContext
    {
        public string[] Instruments { get; set; }

        public string TimeFrame { get; set; }

        public DateTime DefaultStartTime { get; set; }
    }
}
