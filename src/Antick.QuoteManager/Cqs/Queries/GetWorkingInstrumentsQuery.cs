﻿using Antick.Cqrs.Queries;
using Antick.QuoteManager.Components.Agents;
using Antick.QuoteManager.Cqs.Queries.Criterias;

namespace Antick.QuoteManager.Cqs.Queries
{
    public class GetWorkingInstrumentsQuery : IQuery<GetWorkingInstruments, string[]>
    {
        private readonly LoadAgentConfiguration m_Configuration;

        public GetWorkingInstrumentsQuery(LoadAgentConfiguration configuration)
        {
            m_Configuration = configuration;
        }

        public string[] Ask(GetWorkingInstruments criterion)
        {
            return m_Configuration.Instruments;
        }
    }
}
