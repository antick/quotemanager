﻿using System;
using Antick.Cqrs.Queries;

namespace Antick.QuoteManager.Cqs.Queries.Criterias
{
    public class GetQuotesInDay : ICriterion
    {
        public string Instrument { get; set; }

        public DateTime Day { get; set; }
    }
}
