﻿using Antick.Cqrs.Queries;

namespace Antick.QuoteManager.Cqs.Queries.Criterias
{
    /// <summary>
    /// Получение времени последней котировки
    /// </summary>
    public class GetLastTime : ICriterion
    {
        public string Instrument { get; set; }
    }
}
