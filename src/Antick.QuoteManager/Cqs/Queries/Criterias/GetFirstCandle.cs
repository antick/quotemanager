﻿using Antick.Cqrs.Queries;

namespace Antick.QuoteManager.Cqs.Queries.Criterias
{
    public class GetFirstCandle : ICriterion
    {
        public string Instrument { get; set; }
    }
}
