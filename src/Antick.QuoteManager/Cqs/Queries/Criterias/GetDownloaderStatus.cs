﻿using Antick.Cqrs.Queries;

namespace Antick.QuoteManager.Cqs.Queries.Criterias
{
    public class GetDownloaderStatus : ICriterion
    {
        public string Instrument { get; set; }
    }
}
