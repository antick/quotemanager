﻿using Antick.Cqrs.Queries;

namespace Antick.QuoteManager.Cqs.Queries.Criterias
{
    public class GetHistoryLoaderLastTime : ICriterion
    {
        public bool UseCache { get; set; }

        public string Instrument { get; set; }
    }
}
