﻿using System;
using Antick.Cqrs.Queries;

namespace Antick.QuoteManager.Cqs.Queries.Criterias
{
    public class GetBinaryCandlesFrom : ICriterion
    {
        public string Instrument { get; set; }

        public DateTime From { get; set; }
    }
}
