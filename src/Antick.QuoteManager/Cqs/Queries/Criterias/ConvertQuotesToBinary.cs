﻿using System.Collections.Generic;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Queries;

namespace Antick.QuoteManager.Cqs.Queries.Criterias
{
    public class ConvertQuotesToBinary : ICriterion
    {
        public List<Quote> Quotes { get; set; }
    }
}
