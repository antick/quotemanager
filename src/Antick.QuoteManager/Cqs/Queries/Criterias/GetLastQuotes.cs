﻿using Antick.Cqrs.Queries;

namespace Antick.QuoteManager.Cqs.Queries.Criterias
{
    public class GetLastQuotes : ICriterion
    {
        public string Instument { get; set; }

        public int Count { get; set; }
    }
}
