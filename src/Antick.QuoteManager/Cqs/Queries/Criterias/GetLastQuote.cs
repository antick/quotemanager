﻿using Antick.Cqrs.Queries;

namespace Antick.QuoteManager.Cqs.Queries.Criterias
{
    public class GetLastQuote : ICriterion
    {
        public string Instrument { get; set; }

        public bool UseCache { get; set; }
    }
}
