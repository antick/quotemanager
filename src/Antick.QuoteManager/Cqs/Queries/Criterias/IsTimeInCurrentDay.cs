﻿using System;
using Antick.Cqrs.Queries;

namespace Antick.QuoteManager.Cqs.Queries.Criterias
{
    public class IsTimeInCurrentDay : ICriterion
    {
        public DateTime Time { get; set; }
    }
}
