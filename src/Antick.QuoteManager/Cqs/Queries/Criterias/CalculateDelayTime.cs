﻿using Antick.Cqrs.Queries;

namespace Antick.QuoteManager.Cqs.Queries.Criterias
{
    /// <summary>
    /// Критерий вычисления времени ожидания следующей котировки для LoadAgent
    /// </summary>
    public class CalculateDelayTime : ICriterion
    {
        /// <summary>
        /// Режим активной выкачки исторических данных
        /// </summary>
        public bool IsGrubMode { get; set; }

        /// <summary>
        /// Факт скачки хотя бы одной котировки
        /// </summary>
        public bool AnyDownloaded { get; set; }

        /// <summary>
        /// Рабочий Таймфрейм скачки
        /// </summary>
        public string TimeFrame { get; set; }
    }
}
