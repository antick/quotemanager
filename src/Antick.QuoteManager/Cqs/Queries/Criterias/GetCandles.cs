﻿using System;
using Antick.Cqrs.Queries;

namespace Antick.QuoteManager.Cqs.Queries.Criterias
{
    public class GetCandles : ICriterion
    {
        public string Instrument { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public bool UseCache { get; set; }
    }
}
