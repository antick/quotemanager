﻿using System;
using Antick.Cqrs.Queries;

namespace Antick.QuoteManager.Cqs.Queries.Criterias
{
    public class IsTimeInLastDay : ICriterion
    {
        public string Instrument { get; set; }

        public DateTime Time { get; set; }
    }
}
