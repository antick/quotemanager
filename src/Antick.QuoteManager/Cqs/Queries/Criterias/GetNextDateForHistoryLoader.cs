﻿using Antick.Cqrs.Queries;

namespace Antick.QuoteManager.Cqs.Queries.Criterias
{
    public class GetNextDateForHistoryLoader : ICriterion
    {
        public string Instrument { get; set; }
    }
}
