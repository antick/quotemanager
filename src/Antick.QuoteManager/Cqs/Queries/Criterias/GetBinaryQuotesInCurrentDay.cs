﻿using Antick.Cqrs.Queries;

namespace Antick.QuoteManager.Cqs.Queries.Criterias
{
    public class GetBinaryQuotesInCurrentDay : ICriterion
    {
        public string Instrument { get; set; }
    }
}
