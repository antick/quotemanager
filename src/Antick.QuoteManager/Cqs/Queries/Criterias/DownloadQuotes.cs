﻿using System;
using Antick.Cqrs.Queries;

namespace Antick.QuoteManager.Cqs.Queries.Criterias
{
    /// <summary>
    /// Выкачка новых баров по инструменту и тайфрейму с датой больше чем StartTime
    /// </summary>
    public class DownloadQuotes : ICriterion
    {
        public string Instrument { get; set; }

        public string TimeFrame { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime DefaultStartTime { get; set; }
    }
}
