﻿using System;
using System.Linq;
using Antick.Cqrs.Queries;
using Antick.QuoteManager.Components.MemoryCache;
using Antick.QuoteManager.Cqs.Queries.Criterias;

namespace Antick.QuoteManager.Cqs.Queries
{
    public class GetBinaryCandlesFromQuery : IQuery<GetBinaryCandlesFrom, string>
    {
        private readonly IQuotesMemoryCache m_MemoryCache;
        private readonly IQueryBuilder m_QueryBuilder;

        public GetBinaryCandlesFromQuery(IQuotesMemoryCache memoryCache, IQueryBuilder queryBuilder)
        {
            m_MemoryCache = memoryCache;
            m_QueryBuilder = queryBuilder;
        }

        public string Ask(GetBinaryCandlesFrom criterion)
        {
            var startTime = criterion.From.Kind == DateTimeKind.Unspecified
                ? DateTime.SpecifyKind(criterion.From, DateTimeKind.Utc)
                : criterion.From.ToUniversalTime();

            var endTime = new DateTime(criterion.From.Year, criterion.From.Month, criterion.From.Day, 23, 59, 59, DateTimeKind.Utc);
            
            var candles = m_MemoryCache.Get(criterion.Instrument, startTime, endTime);
            var newQuotes = candles.Where(p => p.Time > startTime).ToList();

            if (newQuotes.Any())
            {
                var response = m_QueryBuilder.For<string>().With(new ConvertQuotesToBinary {Quotes = newQuotes});
                return response;
            }

            return string.Empty;
        }
    }
}
