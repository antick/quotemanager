﻿using System;
using System.IO;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Queries;
using Antick.Infractructure.Components.IO;
using Antick.QuoteManager.Cqs.Queries.Criterias;

namespace Antick.QuoteManager.Cqs.Queries
{
    public class ConvertCandlesToBinaryQuery  : IQuery<ConvertQuotesToBinary,string>
    {
        private readonly IListPackage<Quote> m_Package;

        public ConvertCandlesToBinaryQuery(IListPackage<Quote> package)
        {
            m_Package = package;
        }

        public string Ask(ConvertQuotesToBinary criterion)
        {
            var res = m_Package.ConvertToBase64(criterion.Quotes, PackageMode.PackedZip);
            return res;
        }
    }
}
