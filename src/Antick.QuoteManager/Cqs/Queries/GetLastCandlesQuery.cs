﻿using System.Collections.Generic;
using System.Linq;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.QuoteManager.Cqs.Queries.Criterias;
using Antick.QuoteManager.Db;
using Antick.QuoteManager.Db.Dto;
using Antick.QuoteManager.Extensions;

namespace Antick.QuoteManager.Cqs.Queries
{
    public class GetLastCandlesQuery : IQuery<GetLastQuotes, List<Quote>>
    {
        private readonly ISessionBuilder<QuoteContext> m_SessionBuilder;

        public GetLastCandlesQuery(ISessionBuilder<QuoteContext> sessionBuilder)
        {
            m_SessionBuilder = sessionBuilder;
        }

        public List<Quote> Ask(GetLastQuotes criterion)
        {
            return m_SessionBuilder.Execute(session =>
            {
                var dbCandles =
                    session.Query<QuoteDto>()
                        .Where(p => p.Instrument == criterion.Instument)
                        .OrderByDescending(p => p.Time)
                        .Take(criterion.Count)
                        .ToList();

                return dbCandles.Select(x => x.ToContract()).ToList();
            });
        }
    }
}
