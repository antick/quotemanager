﻿using System;
using System.Collections.Generic;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Queries;
using Antick.QuoteManager.Cqs.Queries.Criterias;

namespace Antick.QuoteManager.Cqs.Queries
{
    public class GetCandlesInDayQuery  : IQuery<GetQuotesInDay,List<Quote>>
    {
        private readonly IQueryBuilder m_QueryBuilder;

        public GetCandlesInDayQuery(IQueryBuilder queryBuilder)
        {
            m_QueryBuilder = queryBuilder;
        }

        public List<Quote> Ask(GetQuotesInDay criterion)
        {
            var day = criterion.Day;

            var startDate = new DateTime(day.Year, day.Month, day.Day, 0, 0, 0, DateTimeKind.Utc);
            var endDate = new DateTime(day.Year, day.Month, day.Day, 23, 59, 59, DateTimeKind.Utc);

            return m_QueryBuilder.For<List<Quote>>()
                .With(new GetCandles
                {
                    Instrument = criterion.Instrument,
                    StartTime = startDate,
                    EndTime = endDate,
                    UseCache = true
                });
        }
    }
}
