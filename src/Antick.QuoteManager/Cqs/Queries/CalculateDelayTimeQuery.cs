﻿using System;
using Antick.Cqrs.Queries;
using Antick.Infractructure.Components;
using Antick.Infractructure.Components.Misc;
using Antick.QuoteManager.Cqs.Queries.Criterias;
using Castle.Core.Logging;

namespace Antick.QuoteManager.Cqs.Queries
{
    public class CalculateDelayTimeQuery : IQuery<CalculateDelayTime,TimeSpan>
    {
        private readonly IClock m_Clock;
        private readonly ILogger m_Logger;

        public CalculateDelayTimeQuery(IClock clock, ILogger logger)
        {
            m_Clock = clock;
            m_Logger = logger;
        }

        public TimeSpan Ask(CalculateDelayTime criterion)
        {
            // Если слишком много выкачали повторяем цикл сразу же
            if (criterion.IsGrubMode)
            {
                m_Logger.DebugFormat("Grub mode -> force restart");
                return new TimeSpan(0);
            }

            var now = m_Clock.Now();

            var isHolidays = now.DayOfWeek == DayOfWeek.Saturday ||
                             now.DayOfWeek == DayOfWeek.Sunday;

            if (!isHolidays || criterion.AnyDownloaded)
            {
                var delayTime = getDefaultDelayTime(criterion.TimeFrame);
                if (delayTime.TotalSeconds < 0)
                {
                    m_Logger.WarnFormat("Next delay time was negative. Using default delay time");
                    delayTime = getSpan(criterion.TimeFrame);
                }

                m_Logger.DebugFormat("Await new data in {0} seconds", delayTime.TotalSeconds.ToString("0.00"));

                return delayTime;
            }
            else
            {
                m_Logger.DebugFormat("Today is {0}. Sleep 1 minute", now.DayOfWeek);
                return TimeSpan.FromMinutes(1);
            }
        }

        /// <summary>
        /// Вычисление промежутка ожидания следудующей порции данных согластно таймфрейму
        /// </summary>
        private TimeSpan getDefaultDelayTime(string timeFrame)
        {
            var curData = m_Clock.UtcNow();

            var curDataNoTime = new DateTime(curData.Year, curData.Month, curData.Day, 0, 0, 0, DateTimeKind.Utc);
            var spanSeconds = curData - curDataNoTime;

            var roundSeconds = (int)spanSeconds.TotalSeconds;

            var step = getSecondsInTf(timeFrame);


            int totalSeconds = (roundSeconds / step) * step + step;

            var estimatedData = curDataNoTime.AddSeconds(totalSeconds).AddMilliseconds(500);

            var delayTime = estimatedData - curData;

            return delayTime;
        }

        private TimeSpan getSpan(string TimeFrame)
        {
            switch (TimeFrame)
            {
                case "S5": return new TimeSpan(0, 0, 0, 5);

                default:
                    throw new ArgumentException(string.Format("Span spec for timeframe {0} not defined", TimeFrame));
            }
        }

        private int getSecondsInTf(string timeFrame)
        {
            return (int)getSpan(timeFrame).TotalSeconds;
        }
    }
}
