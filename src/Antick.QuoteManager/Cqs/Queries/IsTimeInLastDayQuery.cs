﻿using System;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Queries;
using Antick.QuoteManager.Cqs.Queries.Criterias;

namespace Antick.QuoteManager.Cqs.Queries
{
    /// <summary>
    /// Определение находится ли указанная дата в последнем дне
    /// </summary>
    public class IsTimeInLastDayQuery : IQuery<IsTimeInLastDay, bool>
    {
        private readonly IQueryBuilder m_QueryBuilder;

        public IsTimeInLastDayQuery(IQueryBuilder queryBuilder)
        {
            m_QueryBuilder = queryBuilder;
        }

        public bool Ask(IsTimeInLastDay criterion)
        {
            var lastDay = m_QueryBuilder.For<Quote>()
                .With(new GetLastQuote {Instrument = criterion.Instrument, UseCache = true});

            if (lastDay == null)
                return true;

            var day = lastDay.Time;
            var start = new DateTime(day.Year, day.Month, day.Day, 0, 0, 0, DateTimeKind.Utc);
            var end = new DateTime(day.Year, day.Month, day.Day, 23, 59, 59, DateTimeKind.Utc);

            var t = criterion.Time.ToUniversalTime();

            // Искомая дата дальше чем последней день
            if (t > end)
                return true;

            return t >= start && t <= end;
        }
    }
}
