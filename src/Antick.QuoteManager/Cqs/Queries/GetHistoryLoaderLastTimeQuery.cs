﻿using System;
using System.Linq;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.QuoteManager.Components.MemoryCache;
using Antick.QuoteManager.Cqs.Queries.Criterias;
using Antick.QuoteManager.Db;
using Antick.QuoteManager.Db.Dto;

namespace Antick.QuoteManager.Cqs.Queries
{
    public class GetHistoryLoaderLastTimeQuery : IQuery<GetHistoryLoaderLastTime, DateTime?>
    {
        private readonly IHistoryLoaderLastTimeMemoryCache m_MemoryCache;
        private readonly ISessionBuilder<QuoteContext> m_SessionBuilder;

        public GetHistoryLoaderLastTimeQuery(IHistoryLoaderLastTimeMemoryCache memoryCache, ISessionBuilder<QuoteContext> sessionBuilder )
        {
            m_MemoryCache = memoryCache;
            m_SessionBuilder = sessionBuilder;
        }

        public DateTime? Ask(GetHistoryLoaderLastTime criterion)
        {
            // достать из кэша если есть
            if (criterion.UseCache)
            {
                var cache = m_MemoryCache.Get(criterion.Instrument);
                if (cache != null) return cache;
            }

            // достать из БД
            var lastTime = m_SessionBuilder.Execute(session =>
            {
                var dto =
                    session.Query<HistoryLoaderStatusDto>().FirstOrDefault(p => p.Instrument == criterion.Instrument);
                return dto?.LastDate;
            });

            // Обновить кэш
            if (lastTime.HasValue)
                m_MemoryCache.Update(criterion.Instrument, lastTime.Value);

            return lastTime;
        }
    }
}
