﻿using System;
using Antick.Cqrs.Queries;
using Antick.Infractructure.Components;
using Antick.Infractructure.Components.Misc;
using Antick.QuoteManager.Cqs.Queries.Criterias;

namespace Antick.QuoteManager.Cqs.Queries
{
    public class IsTimeInCurrentDayQuery : IQuery<IsTimeInCurrentDay, bool>
    {
        private readonly IClock m_Clock;

        public IsTimeInCurrentDayQuery(IClock clock)
        {
            m_Clock = clock;
        }

        public bool Ask(IsTimeInCurrentDay criterion)
        {
            var now = m_Clock.UtcNow();
            var start = new DateTime(now.Year,now.Month,now.Day,0,0,0,DateTimeKind.Utc);
            var end = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59, DateTimeKind.Utc);

            var t = criterion.Time.ToUniversalTime();

            return t >= start && t <= end;
        }
    }
}
