﻿using System;
using System.Linq;
using Antick.Cqrs.Queries;
using Antick.Infractructure.Components;
using Antick.Infractructure.Components.Misc;
using Antick.QuoteManager.Components.MemoryCache;
using Antick.QuoteManager.Cqs.Queries.Criterias;

namespace Antick.QuoteManager.Cqs.Queries
{
    /// <summary>
    /// Выгребает данные из Файлового кэша и добавляет в него данные из кэша памяти (если есть новые )
    /// </summary>
    public class GetBinaryQuotesInCurrentDayQuery : IQuery<GetBinaryQuotesInCurrentDay,string>
    {
        private readonly IClock m_Clock;
        private readonly IQuotesMemoryCache m_MemoryCache;
        private readonly IQueryBuilder m_QueryBuilder;

        public GetBinaryQuotesInCurrentDayQuery(IClock clock,IQuotesMemoryCache memoryCache, IQueryBuilder queryBuilder)
        {
            m_Clock = clock;
            m_MemoryCache = memoryCache;
            m_QueryBuilder = queryBuilder;
        }

        public string Ask(GetBinaryQuotesInCurrentDay criterion)
        {
            var currentDay = m_Clock.UtcNow();

            var startTime = new DateTime(currentDay.Year,currentDay.Month,currentDay.Day,0,0,0,DateTimeKind.Utc);
            var endTime = new DateTime(currentDay.Year, currentDay.Month, currentDay.Day, 23, 59, 59, DateTimeKind.Utc);

            var quotes = m_MemoryCache.Get(criterion.Instrument, startTime, endTime);

            if (quotes.Any())
            {
                var binary = m_QueryBuilder.For<string>().With(new ConvertQuotesToBinary {Quotes = quotes});
                return binary;
            }

            return string.Empty;
        }
    }
}
