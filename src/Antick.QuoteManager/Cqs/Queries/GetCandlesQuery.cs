﻿using System.Collections.Generic;
using System.Linq;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.QuoteManager.Components.MemoryCache;
using Antick.QuoteManager.Cqs.Queries.Criterias;
using Antick.QuoteManager.Db;
using Antick.QuoteManager.Db.Dto;
using Antick.QuoteManager.Extensions;

namespace Antick.QuoteManager.Cqs.Queries
{
    /// <summary>
    /// Получение баров S5 по инструменту в указанном диапозоне
    /// </summary>
    public class GetCandlesQuery : IQuery<GetCandles,List<Quote>>
    {
        private readonly ISessionBuilder<QuoteContext> m_SessionBuilder;
        private readonly IQuotesMemoryCache m_MemoryCache;

        public GetCandlesQuery(ISessionBuilder<QuoteContext> sessionBuilder, IQuotesMemoryCache memoryCache)
        {
            m_SessionBuilder = sessionBuilder;
            m_MemoryCache = memoryCache;
        }

        public List<Quote> Ask(GetCandles criterion)
        {
            // Если разрешено брать из кэша, и указанный промежуток содержится в кэше - берем из кэша
            if (criterion.UseCache && m_MemoryCache.InRange(criterion.Instrument, criterion.StartTime,
                    criterion.EndTime))
            {
                return m_MemoryCache.Get(criterion.Instrument, criterion.StartTime, criterion.EndTime);
            }

            return m_SessionBuilder.Execute(session =>
            {
                var start = criterion.StartTime.ToUniversalTime();
                var end = criterion.EndTime.ToUniversalTime();

                var Quote =
                    session.Query<QuoteDto>().Where(p => p.Instrument == criterion.Instrument && p.Time >= start && p.Time <= end)
                        .OrderBy(p => p.Time)
                        .ToList();

                return Quote.Select(x => x.ToContract()).ToList();
            });
        }
    }
}
