﻿using System.Linq;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.QuoteManager.Components.MemoryCache;
using Antick.QuoteManager.Cqs.Queries.Criterias;
using Antick.QuoteManager.Db;
using Antick.QuoteManager.Db.Dto;
using Antick.QuoteManager.Extensions;

namespace Antick.QuoteManager.Cqs.Queries
{
    public class GetLastQuoteQuery : IQuery<GetLastQuote,Quote>
    {
        private readonly ISessionBuilder<QuoteContext> m_Session;
        private readonly IQuotesMemoryCache m_MemoryCache;

        public GetLastQuoteQuery(ISessionBuilder<QuoteContext> session, IQuotesMemoryCache memoryCache)
        {
            m_Session = session;
            m_MemoryCache = memoryCache;
        }

        public Quote Ask(GetLastQuote criterion)
        {
            // Если разрешено брать из кэша - берещем из него
            if (criterion.UseCache)
            {
                var cache = m_MemoryCache.GetLast(criterion.Instrument);
                if (cache != null)
                    return cache;
            }

            return m_Session.Execute(session =>
            {
                var dto = session.Query<QuoteDto>().Where(p => p.Instrument == criterion.Instrument).OrderByDescending(p => p.Time).FirstOrDefault();
                return dto?.ToContract();
            });
        }
    }
}
