﻿using System;
using System.Collections.Generic;
using System.Linq;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Queries;
using Antick.Infractructure.Components;
using Antick.Infractructure.Components.Misc;
using Antick.Infractructure.Extensions;
using Antick.QuoteManager.Components.FileCache;
using Antick.QuoteManager.Cqs.Queries.Criterias;

namespace Antick.QuoteManager.Cqs.Queries
{
    /// <summary>
    /// Получение бинарных данных за указанный день в разрезе указанного инструмента
    /// </summary>
    public class GetBinaryCandlesInDayQuery : IQuery<GetBinaryCandlesInDay, string>
    {
        private readonly IHistoryCache m_HistoryCache;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly IClock m_Clock;

        public GetBinaryCandlesInDayQuery(IHistoryCache historyCache, IQueryBuilder queryBuilder, IClock clock)
        {
            m_HistoryCache = historyCache;
            m_QueryBuilder = queryBuilder;
            m_Clock = clock;
        }

        public string Ask(GetBinaryCandlesInDay criterion)
        {
            var day = criterion.Day;
            var instrument = criterion.Instrument;

            var d = day.Kind == DateTimeKind.Unspecified ? DateTime.SpecifyKind(day, DateTimeKind.Utc) : day.ToUniversalTime();
            d = new DateTime(d.Year, d.Month, d.Day, 0, 0, 0, DateTimeKind.Utc);
            
            var isFuture = (d - m_Clock.UtcNow()).TotalDays > 0;
            var isCurrentDay = m_QueryBuilder.For<bool>().With(new IsTimeInCurrentDay {Time = d});

            string data;

            // Из прошлого, и не текущий день, и не будущее
            if (!isCurrentDay && !isFuture)
            {
                data = m_HistoryCache.GetBase64(instrument, d).GetSynchronousResult();

                if (data == string.Empty)
                {
                    // Запрашиваемый день закончен, НО еще не окончательно сохранен = выдача промежуточного кэша
                    var candles = m_QueryBuilder.For<List<Quote>>()
                        .With(new GetQuotesInDay { Instrument = instrument, Day = d});
                    if (candles.Any())
                    {
                        data = m_QueryBuilder.For<string>().With(new ConvertQuotesToBinary {Quotes = candles});
                        return data;
                    }
                    else return string.Empty;
                }
                else
                {
                    // Запрашиваемый день закончен = всё ок.
                    return data;
                }

            }

            if (isCurrentDay && !isFuture)
            {
                data = m_QueryBuilder.For<string>().With(new GetBinaryQuotesInCurrentDay {Instrument = instrument});
                // Запрашиваемый день - сегодня, день еще не завершен, ( даннные могут отсутстовать
                return data;
            }

            // Будущий день (нет данных)
            return string.Empty;
        }
    }
}
