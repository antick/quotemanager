﻿using System;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Queries;
using Antick.QuoteManager.Cqs.Queries.Criterias;
using Castle.Core.Logging;

namespace Antick.QuoteManager.Cqs.Queries
{
    public class GetNextDateForHistoryLoaderQuery : IQuery<GetNextDateForHistoryLoader,DateTime?>
    {
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ILogger m_Logger;

        public GetNextDateForHistoryLoaderQuery(IQueryBuilder queryBuilder, ILogger logger)
        {
            m_QueryBuilder = queryBuilder;
            m_Logger = logger;
        }

        public DateTime? Ask(GetNextDateForHistoryLoader criterion)
        {
            DateTime day;

            var lastTime =
                m_QueryBuilder.For<DateTime?>()
                    .With(new GetHistoryLoaderLastTime { Instrument = criterion.Instrument, UseCache = true });

            if (lastTime == null)
            {
                var firstCandle = m_QueryBuilder.For<Quote>().With(new GetFirstCandle { Instrument = criterion.Instrument });

                if (firstCandle == null)
                {
                    m_Logger.DebugFormat("Instrument {0} no data in datebase", criterion.Instrument);
                    return null;
                }

                day = firstCandle.Time;
                day = new DateTime(day.Year, day.Month, day.Day, 0, 0, 0, DateTimeKind.Utc);
            }
            else
            {
                day = lastTime.Value.AddDays(1);
            }

            return day;
        }
    }
}
