﻿using System.Collections.Generic;
using System.Linq;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.QuoteManager.Cqs.Queries.Criterias;
using Antick.QuoteManager.Db;
using Antick.QuoteManager.Db.Dto;
using Antick.QuoteManager.Domain;
using Antick.QuoteManager.Extensions;

namespace Antick.QuoteManager.Cqs.Queries
{
    public class GetHistoryLoaderStatusesQuery : IQuery<GetHistoryLoaderStatuses, List<HistoryLoaderStatus>>
    {
        private readonly ISessionBuilder<QuoteContext> m_SessionBuilder;

        public GetHistoryLoaderStatusesQuery(ISessionBuilder<QuoteContext> sessionBuilder)
        {
            m_SessionBuilder = sessionBuilder;
        }

        public List<HistoryLoaderStatus> Ask(GetHistoryLoaderStatuses criterion)
        {
            return m_SessionBuilder.Execute(session =>
            {
                var statuses = session.Query<HistoryLoaderStatusDto>().ToList();
                return statuses.Select(x => x.ToDomain()).ToList();
            });
        }
    }
}
