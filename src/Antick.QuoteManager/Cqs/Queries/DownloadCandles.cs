﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.Infractructure.Components;
using Antick.Infractructure.Components.Misc;
using Antick.OandaApi;
using Antick.OandaApi.Models;
using Antick.QuoteManager.Cqs.Commands.Context;
using Antick.QuoteManager.Cqs.Queries.Criterias;
using Castle.Core.Logging;

namespace Antick.QuoteManager.Cqs.Queries
{
    public class DownloadQuotesQuery : IQuery<DownloadQuotes,List<Quote>>
    {
        private readonly IOandaClientV20 m_OandaClient;
        private readonly ILogger m_Logger;
        private readonly ICommandBuilder m_CommandBuilder;
        private readonly IClock m_Clock;

        public DownloadQuotesQuery(IOandaClientV20 oandaClient, ILogger logger, ICommandBuilder commandBuilder, IClock clock)
        {
            m_OandaClient = oandaClient;
            m_Logger = logger;
            m_CommandBuilder = commandBuilder;
            m_Clock = clock;
        }

        public List<Quote> Ask(DownloadQuotes criterion)
        {
            var firstLoad = false;

            DateTime? startTime = null;

            if (criterion.StartTime != null)
            {
                startTime = criterion.StartTime;
                m_Logger.DebugFormat("Instrument {0} lastTime is {1} ", criterion.Instrument, criterion.StartTime);
            }
            else
            {
                startTime = DateTime.SpecifyKind(criterion.DefaultStartTime, DateTimeKind.Utc);
                firstLoad = true;
                m_Logger.DebugFormat("Instrument {0} lastTime is not found. Using default {1}", criterion.Instrument, startTime);
            }

            List<CandleMidV20> candles = downloadCandles(criterion.Instrument, criterion.TimeFrame, startTime.Value);

            // удалять повторы только если ранее были данные
            if (!firstLoad)
            {
                // Удаляем повтор
                candles = candles.Where(p => p.Time > startTime).ToList();
            }

            // Записываем данные о скачке в БД
            m_CommandBuilder.Execute(new UpdateDownloderStatusCommandContext
            {
                Instrument = criterion.Instrument,
                Time = m_Clock.UtcNow(),
                StartTime = startTime.Value,
                CountCandles = candles.Count
            });

            if (candles.Count > 0)
            {
                var mapCandles = candles.Select(MapFromAPi).ToList();
                return mapCandles;
            }

            return new List<Quote>();
        }

        /// <summary>
        /// Получение завершенных баров, по инструменту начиная с 1 (первый бар включается)
        /// </summary>
        private List<CandleMidV20> downloadCandles(string instrument, string timeFrame, DateTime startTime)
        {
            try
            {
                var candles = m_OandaClient.GetCandlesMid(instrument, timeFrame,startTime, 5000);
                candles = candles.Where(p => p.Complete).ToList();

                m_Logger.DebugFormat("Instrument {0} downloaded {1} candles", instrument, candles.Count);

                return candles;
            }
            catch (WebException ex)
            {
                m_Logger.Error("Connection error during request Oanda", ex);
                throw;
            }
        }

        private Quote MapFromAPi(CandleMidV20 candleMid)
        {
            return new Quote
            {
                Time = candleMid.Time,
                Open = candleMid.Open,
                Close = candleMid.Close,
                High = candleMid.High,
                Low = candleMid.Low
            };
        }
    }
}
