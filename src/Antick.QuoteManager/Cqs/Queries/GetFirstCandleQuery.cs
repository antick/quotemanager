﻿using System.Linq;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.QuoteManager.Cqs.Queries.Criterias;
using Antick.QuoteManager.Db;
using Antick.QuoteManager.Db.Dto;
using Antick.QuoteManager.Extensions;

namespace Antick.QuoteManager.Cqs.Queries
{
    /// <summary>
    /// Получение первой котировки по инструмену
    /// </summary>
    public class GetFirstCandleQuery : IQuery<GetFirstCandle,Quote>

    {
        private readonly ISessionBuilder<QuoteContext> m_Session;

        public GetFirstCandleQuery(ISessionBuilder<QuoteContext> session)
        {
            m_Session = session;
        }

        public Quote Ask(GetFirstCandle criterion)
        {
            return m_Session.Execute(session =>
            {
                var dto = session.Query<QuoteDto>().Where(p => p.Instrument == criterion.Instrument).OrderBy(p => p.Time).FirstOrDefault();
                return dto?.ToContract();
            });
        }
    }
}
