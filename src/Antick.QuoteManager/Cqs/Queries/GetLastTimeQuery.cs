﻿using System;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Queries;
using Antick.QuoteManager.Cqs.Queries.Criterias;

namespace Antick.QuoteManager.Cqs.Queries
{
    public class GetLastTimeQuery : IQuery<GetLastTime, DateTime?>
    {
        private readonly IQueryBuilder m_QueryBuilder;

        public GetLastTimeQuery(IQueryBuilder queryBuilder)
        {
            m_QueryBuilder = queryBuilder;
        }

        public DateTime? Ask(GetLastTime criterion)
        {
            var lastQuote = m_QueryBuilder.For<Quote>()
                .With(new GetLastQuote { Instrument = criterion.Instrument, UseCache = true });

            DateTime? time = null;
            if (lastQuote != null)
            {
                time = lastQuote.Time;
            }

            return time;
        }
    }
}
