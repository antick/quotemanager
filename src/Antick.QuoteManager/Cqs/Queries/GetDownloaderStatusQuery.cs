﻿using System.Linq;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.QuoteManager.Cqs.Queries.Criterias;
using Antick.QuoteManager.Db;
using Antick.QuoteManager.Db.Dto;
using Antick.QuoteManager.Domain;
using Antick.QuoteManager.Extensions;

namespace Antick.QuoteManager.Cqs.Queries
{
    public class GetDownloaderStatusQuery : IQuery<GetDownloaderStatus,DownloaderStatus>
    {
        private readonly ISessionBuilder<QuoteContext> m_SessionBuilder;

        public GetDownloaderStatusQuery(ISessionBuilder<QuoteContext> sessionBuilder)
        {
            m_SessionBuilder = sessionBuilder;
        }

        public DownloaderStatus Ask(GetDownloaderStatus criterion)
        {
            return m_SessionBuilder.Execute((session) =>
            {
                var dto = session.Query<DownloaderStatusDto>()
                    .FirstOrDefault(p => p.Instrument == criterion.Instrument);

                return dto?.ToDomain();
            });
        }
    }
}
