﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Antick.Contracts;
using Antick.IncApp.QuoteManager.Components.Cache;
using Antick.IncApp.QuoteManager.Components.QuoteDb.DbService;
using Antick.IncApp.QuoteManager.Models;
using Antick.IncApp.QuoteManager.WebApi.Models;
using Org.BouncyCastle.Asn1.Ocsp;

namespace Antick.IncApp.QuoteManager.Components.ApiLogic
{
    public interface IQuoteApiLogic
    {
        Task<Candle> GetFirstCandle(string instument);
        Task<GetDayBinaryResponse> GetDayBinary(string instrument, DateTime day);
        Task<string> GetDayBinaryFrom(string instrument, DateTime from);
    }

    public class QuoteApiLogic : IQuoteApiLogic
    {
        private readonly IQuoteDbService m_QuoteDbService;
        private readonly IHistoryCache m_Cache;
        private readonly IDailyCache m_DailyCache;

        public QuoteApiLogic(IQuoteDbService quoteDbService, IHistoryCache cache, IDailyCache dailyCache)
        {
            m_QuoteDbService = quoteDbService;
            m_Cache = cache;
            m_DailyCache = dailyCache;
        }


        public async Task<Candle> GetFirstCandle(string instument)
        {
            var candleDbModel = await m_QuoteDbService.GetFirstCandle(instument);
            var candle = MapToContract(candleDbModel);
            return candle;
        }

        public async Task<GetDayBinaryResponse> GetDayBinary(string instrument, DateTime day)
        {
            var d = day.Kind == DateTimeKind.Unspecified ? DateTime.SpecifyKind(day, DateTimeKind.Utc) : day.ToUniversalTime();
            d = new DateTime(d.Year, d.Month, d.Day, 0, 0, 0, DateTimeKind.Utc);

            string data;

            var span = DateTime.UtcNow - d;
            var isCurrentDay = span.TotalDays < 1;

            // Из прошлого, и не текущий день, и не будущее
            if (!isCurrentDay && span.TotalDays > 0)
            {
                data = await m_Cache.GetAsBinary(instrument, d);

                if (data == string.Empty)
                {
                    data = await m_DailyCache.GetAsBinary(instrument, d);
                    // Запрашиваемый день закончен, НО еще не окончательно сохранен = выдача промежуточного кэша
                    return new GetDayBinaryResponse {BinaryData = data, Completed = false};
                }
                else
                {
                    // Запрашиваемый день закончен = всё ок.
                    return new GetDayBinaryResponse {BinaryData = data, Completed = true};
                }

            }

            if (isCurrentDay && span.TotalDays > 0)
            {
                data = await m_DailyCache.GetAsBinary(instrument, d);
                // Запрашиваемый день - сегодня, день еще не завершен, ( даннные могут отсутстовать
                return new GetDayBinaryResponse {BinaryData = data, Completed = false};
            }

            // Будущий день (нет данных)
            return new GetDayBinaryResponse {BinaryData = string.Empty, Completed = false};
        }

        public async Task<string> GetDayBinaryFrom(string instrument, DateTime @from)
        {
            var d = from.Kind == DateTimeKind.Unspecified ? DateTime.SpecifyKind(from, DateTimeKind.Utc) : from.ToUniversalTime();

            var data = await m_DailyCache.Get(instrument, d);

            var newQuotes = data.Where(p => p.Time > d).ToList();

            var response = await m_Cache.ConvertToBinary(newQuotes);

            return response;
        }

        private Candle MapToContract(CandleMidModel model)
        {
            if (model == null)
                return null;

            return new Candle
            {
                Time = model.Time,
                Open = model.Open,
                Close = model.Close,
                High = model.High,
                Low = model.Low,
                Complete = true // здесь нет не завершенных
            };
        }
    }
}
