﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Antick.Cqrs.Queries;
using Antick.QuoteManager.Cqs.Queries.Criterias;
using Antick.QuoteManager.Domain;

namespace Antick.QuoteManager.Components.MemoryCache
{
    /// <summary>
    /// Кэш в памяти времени последнего полного кэша дня по каждому инструменту
    /// </summary>
    public class HistoryLoaderLastTimeMemoryCache : IHistoryLoaderLastTimeMemoryCache
    {
        private readonly IQueryBuilder m_QueryBuilder;

        private readonly ConcurrentDictionary<string, DateTime> m_Cache =
            new ConcurrentDictionary<string, DateTime>();

        public HistoryLoaderLastTimeMemoryCache(IQueryBuilder queryBuilder)
        {
            m_QueryBuilder = queryBuilder;
        }

        /// <summary>
        /// Инициализация данными из БД
        /// </summary>
        public void Init()
        {
            var statuses = m_QueryBuilder.For<List<HistoryLoaderStatus>>().With(new GetHistoryLoaderStatuses());
            foreach (var loaderStatus in statuses)
            {
                m_Cache[loaderStatus.Instrument] = loaderStatus.LastTime;
            }
        }
        
        public DateTime? Get(string instrument)
        {
            return m_Cache.ContainsKey(instrument) ? m_Cache[instrument] : (DateTime?) null;
        }

        public void Update(string instrument, DateTime time)
        {
            m_Cache.AddOrUpdate(instrument, time, (s, oldTime) => time);
        }
    }
}
