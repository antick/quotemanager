﻿using System;

namespace Antick.QuoteManager.Components.MemoryCache
{
    public interface IHistoryLoaderLastTimeMemoryCache : IMemoryCache
    {
        DateTime? Get(string instrument);

        void Update(string instrument,DateTime time);
    }
}
