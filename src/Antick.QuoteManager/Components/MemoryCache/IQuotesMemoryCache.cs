﻿using System;
using System.Collections.Generic;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;

namespace Antick.QuoteManager.Components.MemoryCache
{
    public interface IQuotesMemoryCache : IMemoryCache
    {
        /// <summary>
        /// Запись новых данных в кэш
        /// </summary>
        void Update(string instrument,List<Quote> newQuotes);

        /// <summary>
        /// Получение последнего по этому инструменту
        /// </summary>
        Quote GetLast(string instrument);

        /// <summary>
        /// Определение нахождения временного диапозона  в 
        /// </summary>
        bool InRange(string instrument,DateTime startTime, DateTime endTime);

        /// <summary>
        /// Получение данных в указанном диаопозоне
        /// </summary>
        List<Quote> Get(string instrument, DateTime startTime, DateTime endTime);


        List<Quote> GetGreater(string instrument, DateTime starTime);
    }
}
