﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Queries;
using Antick.Infractructure.Components;
using Antick.Infractructure.Components.Misc;
using Antick.QuoteManager.Components.Agents;
using Antick.QuoteManager.Cqs.Queries.Criterias;

namespace Antick.QuoteManager.Components.MemoryCache
{
    public class QuotesMemoryCache : IQuotesMemoryCache
    {
        private readonly LoadAgentConfiguration m_Configuration;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly IClock m_Clock;

        private readonly ConcurrentDictionary<string, List<Quote>> m_Cache =
            new ConcurrentDictionary<string, List<Quote>>();
        
        /// <summary>
        /// Максимальное кол-во баров S5 в одном дне 
        /// </summary>
        private const int QUOTES_COUNT = 17280;


        public QuotesMemoryCache(LoadAgentConfiguration configuration, IQueryBuilder queryBuilder, IClock clock)
        {
            m_Configuration = configuration;
            m_QueryBuilder = queryBuilder;
            m_Clock = clock;
        }

        public void Init()
        {
            foreach (var instrument in m_Configuration.Instruments)
            {
                m_Cache[instrument] = m_QueryBuilder.For<List<Quote>>()
                    .With(new GetLastQuotes {Instument = instrument, Count = QUOTES_COUNT});
            }
        }

        public void Update(string instrument, List<Quote> newQuotes)
        {
            List<Quote> allQuotes = m_Cache[instrument];
            allQuotes.AddRange(newQuotes);
            allQuotes = allQuotes.OrderByDescending(p => p.Time).Take(QUOTES_COUNT).OrderBy(p => p.Time).ToList();

            m_Cache.AddOrUpdate(instrument, newQuotes, (s, list) => allQuotes);
        }

        public Quote GetLast(string instrument)
        {
            if (m_Cache.ContainsKey(instrument))
                return m_Cache[instrument].OrderByDescending(p => p.Time).FirstOrDefault();

            return null;
        }

        public bool InRange(string instrument, DateTime startTime, DateTime endTime)
        {
            if (m_Cache.ContainsKey(instrument) && m_Cache[instrument].Any(p => p.Time == startTime))
            {
                // Верхняя граница в будущем
                if (endTime.ToUniversalTime() >= m_Clock.UtcNow())
                    return true;

                else return m_Cache[instrument].Any(p => p.Time >= endTime);
            }
            else return false;
        }

        public List<Quote> Get(string instrument, DateTime startTime, DateTime endTime)
        {
            if (m_Cache.ContainsKey(instrument))
                return m_Cache[instrument].Where(p => p.Time >= startTime & p.Time <= endTime).OrderBy(p => p.Time)
                    .ToList();

            return new List<Quote>();
        }

        public List<Quote> GetGreater(string instrument, DateTime starTime)
        {
            return m_Cache.ContainsKey(instrument)
                ? m_Cache[instrument].Where(p => p.Time > starTime).OrderBy(p => p.Time).ToList()
                : new List<Quote>();
        }
    }
}
