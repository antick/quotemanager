﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Infractructure.Components.IO;

namespace Antick.QuoteManager.Components.FileCache
{ 
    public class HistoryCache : IHistoryCache
    {
        private readonly string m_Folder;
        private readonly IListPackage<Quote> m_Package;

        private const string FILE_EXTENSION = "dat";

        public HistoryCache(string folder, IListPackage<Quote> package)
        {
            m_Folder = folder;
            m_Package = package;
        }

        private string computeFullPath(string instrument, DateTime day)
        {
            var curDir = Directory.GetCurrentDirectory();
            var baseDir = $"{curDir}\\{m_Folder}";

            var dir = $"{baseDir}\\{instrument}\\{day.Year}";

            return dir + "\\" + $"{day:yyyy-MM-dd}.{FILE_EXTENSION}";
        }

        /// <summary>
        /// Чтение из файла с распаковкой и десериализацией
        /// </summary>
        /// <param name="fullPath">полный путь до файла</param>
        /// <returns>Распакованные данные</returns>
        public async Task<List<Quote>> Get(string fullPath)
        {
            if (!File.Exists(fullPath))
                return null;
            
            var data = m_Package.Read(fullPath, PackageMode.PackedZip);
            return await Task.FromResult(data);
        }

        /// <summary>
        /// Чтение в Base64 из указанного файла
        /// </summary>
        /// <param name="filePath">Полный путь до файла</param>
        /// <returns></returns>
        protected async Task<string> GetBase64(string filePath)
        {
            if (!File.Exists(filePath))
                return await Task.FromResult(string.Empty);

            return m_Package.ReadAsBase64(filePath);
        }

        /// <summary>
        /// Сохранение данных с упаковкой и сериализацией
        /// </summary>
        /// <param name="quotes">Данные</param>
        /// <param name="fullpath"></param>
        /// <returns></returns>
        public async Task<bool> Save(List<Quote> quotes, string fullpath)
        {
            if (!Directory.Exists(fullpath))
            {
                var dir = Path.GetDirectoryName(fullpath);
                Directory.CreateDirectory(dir);
            }

            m_Package.Write(quotes, fullpath, PackageMode.PackedZip);

            return await Task.FromResult(true);
        }

        public async Task<List<Quote>> Get(string instrument, DateTime date)
        {
            return await Get(computeFullPath(instrument, date));
        }

        public async Task<string> GetBase64(string instrument, DateTime date)
        {
            return await GetBase64(computeFullPath(instrument, date));
        }

        public async Task<bool> Save(List<Quote> quotes, string instrument, DateTime date)
        {
            return await Save(quotes, computeFullPath(instrument, date));
        }
    }
}
