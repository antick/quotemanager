﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;

namespace Antick.QuoteManager.Components.FileCache
{
    public interface IHistoryCache
    {
        /// <summary>
        /// Сохранить кэш полного дня
        /// </summary>
        Task<bool> Save(List<Quote> quotes, string instrument, DateTime date);

        /// <summary>
        /// Получить десериализованный данные за указанный день по указанному инструменту
        /// </summary>
        Task<List<Quote>> Get(string instrument, DateTime date);

        /// <summary>
        /// Получить исходные бинарные данные за указанный день по указанному инструменту
        /// </summary>
        Task<string> GetBase64(string instrument, DateTime date);
    }
}
