﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.IncApp.QuoteManager.Components.Cache
{
    public abstract class CacheServiceBase
    {
        /// <summary>
        /// Главный каталог кеша
        /// </summary>
        /// <returns></returns>
        protected string folderBase()
        {
            var curDir = Directory.GetCurrentDirectory();
            var dir = $"{curDir}\\Data\\Cache";
            return dir;
        }

        protected void CheckDirectory(string directory)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
        }

        protected void DeleteFile(string fullPath)
        {
            if (File.Exists(fullPath))
                File.Delete(fullPath);
        }
    }
}
