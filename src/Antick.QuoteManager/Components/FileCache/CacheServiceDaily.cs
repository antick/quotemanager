﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts;
using Antick.IncApp.QuoteManager.Components.Cache;
using Antick.Integration.Internals.IO;

namespace Antick.IncApp.QuoteManager.Components
{
    public interface ICacheServiceDaily
    {
        /// <summary>
        /// Сохранить кэш текущего незаконченного дня
        /// </summary>
        Task<bool> Save(List<CandleLight> candles, string instrument);

        /// <summary>
        /// Сохранить кэш текущего дня как промежуточный(до формирования полного)
        /// </summary>
        /// <param name="candles"></param>
        /// <param name="instrument"></param>
        /// <returns></returns>
        Task<bool> SaveTmp(List<CandleLight> candles, string instrument);


        Task<List<CandleLight>> Get(string instrument, DateTime day);

        Task<string> GetAsBinary(string instrument, DateTime day);
        Task<string> GetTmpAsBinary(string instrument, DateTime date);

        Task<bool> DeleteTmp(string instrument, DateTime date);
    }

    public class CacheServiceDaily : CacheServiceBase, ICacheServiceDaily
    {
        private string folder()
        {
            return folderBase() + "\\" + "Daily";
        }

        private string fileDirectoryDaily()
        {
            var dir = $"{folder()}";
            return dir;
        }

        private string fileNameDaily(string insturment, DateTime day)
        {
            return $"{insturment}_{day.Year}.{day.Month}.{day.Day:00}.dat";
        }

        private string fileNameDailyTmp(string insturment, DateTime day)
        {
            return $"{insturment}_{day.Year}.{day.Month}.{day.Day:00}_tmp.dat";
        }

        private string fullPathDaily(string instrument, DateTime day)
        {
            return fileDirectoryDaily() + "\\" + fileNameDaily(instrument,day);
        }

        private string fullPathDailyTmp(string instrument, DateTime day)
        {
            return fileDirectoryDaily() + "\\" + fileNameDailyTmp(instrument, day);
        }


        public async Task<bool> Save(List<CandleLight> candles, string instrument)
        {
            var day = candles.First().Time;

            var fileDirectory = fileDirectoryDaily();
            CheckDirectory(fileDirectory);

            var fullPath = fullPathDaily(instrument, day);
            DeleteFile(fullPath);

            var cache = new CompressHardCache<CandleLight>();
            cache.Write(fullPath, candles);

            return await Task.FromResult(true);
        }

        public async Task<bool> SaveTmp(List<CandleLight> candles, string instrument)
        {
            var day = candles.First().Time;

            var fileDirectory = fileDirectoryDaily();
            CheckDirectory(fileDirectory);

            var fullPath = fullPathDailyTmp(instrument, day);
            DeleteFile(fullPath);

            var cache = new CompressHardCache<CandleLight>();
            cache.Write(fullPath, candles);

            return await Task.FromResult(true);
        }

        public async Task<List<CandleLight>> Get(string instrument,DateTime day)
        {
            var fullpath = fullPathDaily(instrument, day);
            if (!File.Exists(fullpath))
                return null;

            var cache = new CompressHardCache<CandleLight>();
            var data = cache.Read(fullpath);
            return await Task.FromResult(data);
        }

        public async Task<string> GetTmpAsBinary(string instrument, DateTime date)
        {
            var fullpath = fullPathDailyTmp(instrument, date);
            if (!File.Exists(fullpath))
                return await Task.FromResult(string.Empty);

            using (FileStream fs = new FileStream(fullpath, FileMode.Open))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    byte[] bin = br.ReadBytes(Convert.ToInt32(fs.Length));
                    return await Task.FromResult(Convert.ToBase64String(bin));
                }
            }
        }

        public async Task<string> GetAsBinary(string instrument, DateTime day)
        {
            var fullpath = fullPathDaily(instrument,day);
            if (!File.Exists(fullpath))
                return await Task.FromResult(string.Empty);

            using (FileStream fs = new FileStream(fullpath, FileMode.Open))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    byte[] bin = br.ReadBytes(Convert.ToInt32(fs.Length));
                    return await Task.FromResult(Convert.ToBase64String(bin));
                }
            }
        }

        public async Task<bool> DeleteTmp(string instrument, DateTime date)
        {
            var fileDirectory = fileDirectoryDaily();
            var fullPath = fileDirectory + "\\" + fileNameDailyTmp(instrument, date);

            if (File.Exists(fullPath))
                File.Delete(fullPath);

            return await Task.FromResult(true);
        }
    }
}
