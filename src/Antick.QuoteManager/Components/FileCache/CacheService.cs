﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Antick.Contracts;
using Antick.IncApp.QuoteManager.Models;
using Antick.Integration.Internals.IO;
using Raven.Abstractions.Extensions;
using Antick.IncApp.QuoteManager.Components.Cache;

namespace Antick.IncApp.QuoteManager.Components
{
    public interface ICacheService
    {
        /// <summary>
        /// Сохранить кэш полного дня
        /// </summary>
        Task<bool> Save(List<CandleLight> candles, string instrument, DateTime date);

       

        Task<string> SaveAsBase64String(List<CandleLight> candles);
        Task<List<CandleLight>> Get(string instrument,DateTime date);
      
        
        Task<string> GetAsBase64String(string instrument, DateTime date);
       
        
        Task<bool> Exists(string instrument, DateTime date);

    
    }

    public class CacheService : CacheServiceBase, ICacheService
    {
        private string folder()
        {
            return folderBase() + "\\" + "Completed";
        }

        private string fileDirectory(string instrument, DateTime day)
        {
            var dir = $"{folder()}\\{instrument}\\{day.Year}\\{day.Month}";
            return dir;
        }
        
        private string fileName(DateTime day)
        {
            return $"{day.Day:00}.dat";
        }      

        private string fullPath(string instrument, DateTime day)
        {
            return fileDirectory(instrument,day) + "\\" + fileName(day);
        }    
        
        public async Task<bool> Save(List<CandleLight> candles, string instrument, DateTime date)
        {
            var fileDirectory = this.fileDirectory(instrument, date);
            CheckDirectory(fileDirectory);

            var fullPath = fileDirectory + "\\" + fileName(date);
            if (File.Exists(fullPath))
                File.Delete(fullPath);

            var cache = new CompressHardCache<CandleLight>();
            cache.Write(fullPath,candles);

            return await Task.FromResult(true);
        }

        

        public async Task<string> SaveAsBase64String(List<CandleLight> candles)
        {
            using (var memoryStream = new MemoryStream())
            {
                var cache = new CompressHardCache<CandleLight>();
                cache.Write(memoryStream,candles);

                memoryStream.Position = 0;

                var array = memoryStream.ToArray();

                return Convert.ToBase64String(array);
            }
          
        }

        public async Task<List<CandleLight>> Get(string instrument, DateTime date)
        {
            var fullpath = fullPath(instrument, date);
            if (!File.Exists(fullpath))
                return null;

            var cache = new CompressHardCache<CandleLight>();
            var data = cache.Read(fullpath);
            return await Task.FromResult(data);
        }

        

        public async Task<string> GetAsBase64String(string instrument, DateTime date)
        {
            var fullpath = fullPath(instrument, date);
            if (!File.Exists(fullpath))
                return await Task.FromResult(string.Empty);

            using (FileStream fs = new FileStream(fullpath, FileMode.Open))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    byte[] bin = br.ReadBytes(Convert.ToInt32(fs.Length));
                    return await Task.FromResult(Convert.ToBase64String(bin));
                }
            }

        }

   

        public async Task<bool> Exists(string instrument, DateTime date)
        {
            var fullpath = fullPath(instrument, date);
            return await Task.FromResult(File.Exists(fullpath));
        }

        
    }
}
