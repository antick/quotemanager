﻿using System;

namespace Antick.QuoteManager.Components.Agents
{
    public class LoadAgentConfiguration
    {
        public string[] Instruments { get; set; }
        public DateTime StartTime { get; set; }
        public string TimeFrame { get; set; }
    }
}
