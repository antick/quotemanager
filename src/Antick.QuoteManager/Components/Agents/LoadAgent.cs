﻿using System;
using Antick.Cqrs.Handlers;
using Antick.Infractructure.Components.Agent;
using Antick.QuoteManager.Cqs.Handlers.HandlerContext;
using Castle.Core.Logging;

namespace Antick.QuoteManager.Components.Agents
{
    public class LoadAgent : BackAgent
    {
        protected override TimeSpan ErrorDelayTime => new TimeSpan(0, 0, 5);

        private readonly IHandlerBuilder m_HandlerBuilder;
        private readonly LoadAgentConfiguration m_Configuration;


        public LoadAgent(ILogger logger, LoadAgentConfiguration configuration, IHandlerBuilder handlerBuilder) : base(logger)
        {
            m_HandlerBuilder = handlerBuilder;
            m_Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        protected override TimeSpan? Execute()
        {
            var delayTime = m_HandlerBuilder.For<TimeSpan>()
                .With(new LoadAgentHandlerContext
                {
                    Instruments = m_Configuration.Instruments,
                    TimeFrame = m_Configuration.TimeFrame,
                    DefaultStartTime = m_Configuration.StartTime
                });

            return delayTime;
        }

        
    }
}