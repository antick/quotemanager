﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Antick.Contracts;
using Antick.Contracts.Messaging.QuoteManager;
using Antick.IncApp.QuoteManager.Components.Agents.Load;
using Antick.IncApp.QuoteManager.Components.QuoteDb.DbService;
using Antick.Integration.Internals.Extensions;
using Antick.Integration.Internals.Misc;
using Castle.Core.Logging;
using EasyNetQ;

namespace Antick.IncApp.QuoteManager.Components.Agents
{
    public class QuoteDailyCacheAgent : BackAgent
    {
        private readonly IQuoteDbService m_QuoteDbService;
        private readonly QuoteLoadAgentConfiguration m_QuoteLoadAgentConfiguration;
        private readonly ICacheServiceDaily m_CacheServiceDaily;
        private readonly QuoteLoadAgent m_QuoteLoadAgent;
        private readonly IBus m_Bus;
        private readonly IClock m_Clock;

        private readonly object m_LockObj = new object();

        public QuoteDailyCacheAgent(QuoteLoadAgentConfiguration quoteLoadAgentConfiguration,
            IQuoteDbService quoteDbService, ILogger logger, ICacheServiceDaily cacheServiceDaily,
            QuoteLoadAgent quoteLoadAgent, IBus bus, IClock clock) : base(logger)
        {
            m_QuoteLoadAgentConfiguration = quoteLoadAgentConfiguration;
            m_QuoteDbService = quoteDbService;
            m_CacheServiceDaily = cacheServiceDaily;
            m_QuoteLoadAgent = quoteLoadAgent;
            m_Bus = bus;
            m_Clock = clock;
            m_QuoteLoadAgent.QuoteLoaded += quoteLoadAgentQuoteLoaded;
        }

        protected override TimeSpan? Execute()
        {
            iteration(m_QuoteLoadAgentConfiguration.Instruments.ToList());
            // Одиночный запуск
            return null;
        }

        private void quoteLoadAgentQuoteLoaded(object sender, QuoteLoadedEventArgs e)
        {
            Logger.DebugFormat("Hanled event NewQuoteDownloaded: {0}", e.Instrument);
            iteration(new List<string> {e.Instrument});
        }

        private void iteration(List<string> instruments)
        {
            lock (m_LockObj)
            {
                foreach (var instrument in instruments)
                {
                    var d = m_Clock.UtcNow();

                    DateTime? lastDate = null;
                    var prevData = m_CacheServiceDaily.Get(instrument, d).GetSynchronousResult();
                    if (prevData != null && prevData.Any())
                    {
                        if (prevData.First().Time.Day == d.Day)
                        {
                            lastDate = prevData.Last().Time;
                            Logger.DebugFormat("Instrument {0} - prevCache is active", instrument);
                        }
                        else
                        {
                            Logger.DebugFormat("Instrument {0} prevCache is old. Deleting", instrument);

                            // Сохранение промежуточного кэша
                            m_CacheServiceDaily.SaveTmp(prevData, instrument);


                            lastDate = null;
                            prevData = new List<CandleLight>();
                        }
                    }
                    else prevData = new List<CandleLight>();

                    if (lastDate == null)
                    {
                        lastDate = new DateTime(d.Year, d.Month, d.Day, 0, 0, 0, DateTimeKind.Utc);
                    }
                    var endDate = new DateTime(d.Year, d.Month, d.Day, 23, 59, 59, DateTimeKind.Utc);

                    Logger.DebugFormat("Instrument {0} download from db new data", instrument);

                    var newData = m_QuoteDbService.GetCandles(instrument, lastDate.Value, endDate)
                        .GetSynchronousResult();

                    prevData.AddRange(newData.Select(p => p.ToCacheModel()));

                    if (!prevData.Any())
                    {
                        Logger.DebugFormat("Instrument {0} nothing to save", instrument);
                        continue;
                    }

                    m_CacheServiceDaily.Save(prevData, instrument).GetSynchronousResult();
                    Logger.DebugFormat("Instrument {0} saved", instrument);

                    // Публикация сообщения о новой котировке
                    m_Bus.Publish(new NewQuoteReceived {Instrument = instrument, Time = prevData.Last().Time});
                }
            }
        }
    }
}
