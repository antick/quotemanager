﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Antick.IncApp.QuoteManager.Components.Agents.Load;
using Antick.IncApp.QuoteManager.Components.QuoteDb.DbService;
using Antick.IncApp.QuoteManager.Components.QuoteDb.Ef;
using Antick.Integration.Internals.Extensions;
using Antick.Integration.Internals.Misc;
using Castle.Core.Logging;

namespace Antick.IncApp.QuoteManager.Components.Agents
{
    /// <summary>
    /// Агент по сохранению запакованных в ZIP файликах баров по каждому символу за каждый день на диск
    /// </summary>
    public class QuoteCacheAgent : BackAgent
    {
        protected override TimeSpan ErrorDelayTime => new TimeSpan(0, 1, 0);

        private readonly IQuoteDbService m_QuoteDbService;
        private readonly QuoteLoadAgentConfiguration m_QuoteLoadAgentConfiguration;
        private readonly ICacheService m_CacheService;
        private readonly ICacheServiceDaily m_CacheServiceDaily;
        private readonly IClock m_Clock;

        private List<QuoteFileLoaderStatus> m_Statuses;

        public QuoteCacheAgent(ILogger logger, IQuoteDbService quoteDbService,
            QuoteLoadAgentConfiguration quoteLoadAgentConfiguration, ICacheService cacheService,
            ICacheServiceDaily cacheServiceDaily, IClock clock):base(logger)
        {
            m_QuoteDbService = quoteDbService;
            m_QuoteLoadAgentConfiguration = quoteLoadAgentConfiguration;
            m_CacheService = cacheService;
            m_CacheServiceDaily = cacheServiceDaily;
            m_Clock = clock;
        }

        protected override void Load()
        {
            m_Statuses = m_QuoteDbService.GetQuoteFileLoaderStatuses().GetSynchronousResult();
        }

        protected override TimeSpan? Execute()
        {
            List<bool> isNeedSleep = new List<bool>();

            foreach (var instrument in m_QuoteLoadAgentConfiguration.Instruments)
            {
                var status = m_Statuses.FirstOrDefault(p => p.Instrument == instrument);
                Logger.DebugFormat("Instrument {0} - loading new cache for next day", instrument);
                var saved = load(instrument, status);
                isNeedSleep.Add(!saved);
            }

            // Если все скачали то спим все
            if (isNeedSleep.Count > 0 && isNeedSleep.All(p => p))
            {
                Logger.DebugFormat("Sleep 1 minute");
                return TimeSpan.FromMinutes(1);
            }

            return new TimeSpan(0,0,0);
        }

        private bool load(string instrument, QuoteFileLoaderStatus status)
        {
            DateTime day;

            if (status == null)
            {
                var firstCandle = m_QuoteDbService.GetFirstCandle(instrument).GetSynchronousResult();
                day = firstCandle.Time;
                day = new DateTime(day.Year,day.Month,day.Day,0,0,0,DateTimeKind.Utc);
            }
            else
            {
                day = status.LastDate.AddDays(1);
            }

            var span = m_Clock.UtcNow() - day;
            var isCurrentDay = span.TotalDays < 1;
            if (isCurrentDay)
            {
                Logger.DebugFormat("Intstrument {0} day {1} is not completed", instrument, day.ToString("yyyy/MM/dd"));
                return false;
            }

            var startDate = new DateTime(day.Year,day.Month,day.Day,0,0,0,DateTimeKind.Utc);
            var endDate = new DateTime(day.Year, day.Month, day.Day, 23, 59, 59, DateTimeKind.Utc);

            // Выкачка
            var candles = m_QuoteDbService.GetCandles(instrument, startDate, endDate).GetSynchronousResult();

            if (!candles.Any())
            {
                Logger.DebugFormat("Intstrument {0} no candles for day {1}", instrument, day.ToString("yyyy/MM/dd"));
                status = updateLocalStatus(status, instrument, day);
                m_QuoteDbService.SaveQuoteFileLoaderStatus(status).GetSynchronousResult();
                return false;
            }

            // Удаление промежуточных данных с диска
            m_CacheServiceDaily.DeleteTmp(instrument, day).GetSynchronousResult();

            // Сохранение на диск
            m_CacheService.Save(candles.Select(x => x.ToCacheModel()).ToList(),instrument,day).GetSynchronousResult();


            Logger.DebugFormat("Intstrument {0} saved for day {1}",instrument, day.ToString("yyyy/MM/dd"));
            
            status = updateLocalStatus(status, instrument, day);
            m_QuoteDbService.SaveQuoteFileLoaderStatus(status).GetSynchronousResult();

            return true;
        }

        private QuoteFileLoaderStatus updateLocalStatus(QuoteFileLoaderStatus status, string instrument, DateTime day)
        {
            if (status == null)
            {
                status = new QuoteFileLoaderStatus() { Instrument = instrument, LastDate = day };
                m_Statuses.Add(status);
            }
            else status.LastDate = day;


            return status;
        }
    }
}