﻿using System;

namespace Antick.IncApp.QuoteManager.Components.Agents.Load
{
    public class QuoteLoadAgentConfiguration
    {
        public string[] Instruments { get; set; }
        public DateTime StartTime { get; set; }
        public string TimeFrame { get; set; }
        public bool DebugLogs { get; set; }
    }
}
