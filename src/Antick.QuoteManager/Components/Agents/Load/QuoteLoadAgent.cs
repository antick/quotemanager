﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Antick.IncApp.QuoteManager.Components.QuoteDb.DbService;
using Antick.IncApp.QuoteManager.Models;
using Antick.Integration.Internals.Extensions;
using Antick.Integration.Internals.Misc;
using Antick.OandaApi;
using Antick.OandaApi.Configuration;
using Antick.OandaApi.DataTypes;
using Castle.Core.Logging;
using EasyNetQ;
using Candle = Antick.Contracts.Candle;

namespace Antick.IncApp.QuoteManager.Components.Agents.Load
{
    /// <summary>
    /// Агент по регулярной выгрузки баров из Oanda
    /// </summary>
    public interface IQuoteLoadAgent : IBackAgent
    {
        event EventHandler<QuoteLoadedEventArgs> QuoteLoaded;
    }

    public class QuoteLoadAgent : BackAgent, IQuoteLoadAgent
    {
        public event EventHandler<QuoteLoadedEventArgs> QuoteLoaded;

        protected override TimeSpan ErrorDelayTime => new TimeSpan(0, 0, 5);

        private readonly IClock m_Clock;
        private readonly QuoteLoadAgentConfiguration m_Configuration;
        private readonly IOandaApiFactory m_OandaApiFactory;
        private readonly IQuoteDbService m_QuoteDbService;
        private readonly IBus m_Bus;
        
        /// <summary>
        /// Кол-во баров, при превышении которорго считаем что нужно без задержки дергать АПИ (не догружены исторические котировки)
        /// </summary>
        private const int GRUB_COUNT = 5;
        
        public QuoteLoadAgent(ILogger logger, QuoteLoadAgentConfiguration configuration,
            IOandaApiFactory oandaApiFactory, IQuoteDbService quoteDbService, IBus bus, IClock clock) : base(logger)
        {
            m_Clock = clock;
            m_Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            m_OandaApiFactory = oandaApiFactory ?? throw new ArgumentNullException(nameof(oandaApiFactory));
            m_QuoteDbService = quoteDbService ?? throw new ArgumentNullException(nameof(quoteDbService));
            m_Bus = bus ?? throw new ArgumentNullException(nameof(bus));
        }

        protected override TimeSpan? Execute()
        {
            var grubMode = false;
            var anyDownloaded = false;

            foreach (var instrument in m_Configuration.Instruments)
            {
                int downloadCandles = iterate(instrument);
                grubMode |= downloadCandles > GRUB_COUNT;
                if (downloadCandles > 0)
                {
                    anyDownloaded = true;
                }
            }

            return GetDelayTime(grubMode, anyDownloaded);
        }

        private int iterate(string instrument)
        {
            var lastCandle = m_QuoteDbService.GetLastCandle(instrument, m_Configuration.TimeFrame)
                .GetSynchronousResult();

            DateTime? startTime = null;
            if (lastCandle != null)
            {
                startTime = lastCandle.Time;
            }

            return downloadAndSave(instrument, startTime);
        }

        public TimeSpan GetDelayTime(bool grubMode, bool anyDownloaded)
        {
            // Если слишком много выкачали повторяем цикл сразу же
            if (grubMode)
            {
                debugFormat("Grub mode -> force restart");
                return new TimeSpan(0);
            }

            var now = m_Clock.Now();

            var isHolidays = now.DayOfWeek == DayOfWeek.Saturday ||
                             now.DayOfWeek == DayOfWeek.Sunday;

            if (!isHolidays || anyDownloaded)
            {
                var delayTime = GetDefaultDelayTime(m_Configuration.TimeFrame);
                if (delayTime.TotalSeconds < 0)
                {
                    Logger.WarnFormat("Next delay time was negative. Using default delay time");
                    delayTime = getSpan(m_Configuration.TimeFrame);
                }

                debugFormat("Await new data in {0} seconds", delayTime.TotalSeconds.ToString("0.00"));

                return delayTime;
            }
            else
            {
                Logger.DebugFormat("Today is {0}. Sleep 1 minute", now.DayOfWeek);
                return TimeSpan.FromMinutes(1);
            }
        }

        /// <summary>
        /// Вычисление промежутка ожидания следудующей порции данных согластно таймфрейму
        /// </summary>
        public TimeSpan GetDefaultDelayTime(string timeFrame)
        {
            var curData = m_Clock.UtcNow();

            var curDataNoTime = new DateTime(curData.Year, curData.Month, curData.Day, 0, 0, 0, DateTimeKind.Utc);
            var spanSeconds = curData - curDataNoTime;

            var roundSeconds = (int)spanSeconds.TotalSeconds;

            var step = getSecondsInTf(timeFrame);


            int totalSeconds = (roundSeconds / step) * step + step;

            var estimatedData = curDataNoTime.AddSeconds(totalSeconds).AddMilliseconds(500);

            var delayTime = estimatedData - curData;

            return delayTime;
        }

        /// <summary>
        /// Выкачка и сохранение данные по инструмету, пока всё не выкачаем
        /// </summary>
        private int downloadAndSave(string instrument, DateTime? startTime)
        {
            var firstLoad = false;

            if (startTime != null)
            {
                debugFormat("Instrument {0} lastTime is {1} ", instrument, startTime);
            }
            else
            {
                startTime = DateTime.SpecifyKind(m_Configuration.StartTime, DateTimeKind.Utc);
                firstLoad = true;
                debugFormat("Instrument {0} lastTime is not found. Using default {1}", instrument, startTime);
            }

            List<CandleMid> candles = getCompletedCandles(instrument, startTime.Value);

            // удалять повторы только если ранее были данные
            if (!firstLoad)
            {
                // Удаляем повтор
                candles = candles.Where(p => p.Time > startTime).ToList();
            }

            if (candles.Count > 0)
            {

                var mapCandles = candles.Select(x => MapFromAPi(x, instrument, m_Configuration.TimeFrame)).ToList();
                var taks = m_QuoteDbService.SaveCandles(mapCandles);
                taks.Wait();

                debugFormat("Instrument {0} save to db candles from {1} candles count {2}, lastTime is {3}", instrument,
                    startTime, mapCandles.Count, mapCandles.Last().Time.ToString("s"));

                //var message = new Contracts.Messaging.QuoteManager.Quote
                //{
                // Instrument = instrument,
                // Candles = candles.Select(Map).ToList(),
                // CreatedTime = m_Clock.UtcNow()
                //};
                // Publish event - To Rabbit
                //m_Bus.Publish(message);

                // Publish LocalEvent
                QuoteLoaded.Raise(this, new QuoteLoadedEventArgs { Instrument = instrument });
            }
            else
            {
                debugFormat("Instrument {0} no new completed candles", instrument);
            }

            return candles.Count;
        }

        /// <summary>
        /// Получение завершенных баров, по инструменту начиная с 1 (первый бар включается)
        /// </summary>
        private List<CandleMid> getCompletedCandles(string instrument, DateTime startTime)
        {
            IOandaClient apiClient = null;
            List<CandleMid> candles = null;

            try
            {
                apiClient =
                    m_OandaApiFactory.Create(new OandaContext()
                    {
                        Instrument = instrument,
                        Granularity = m_Configuration.TimeFrame
                    });

                candles = apiClient.GetCandlesMid(startTime, 5000);
                candles = candles.Where(p => p.Complete).ToList();

                debugFormat("Instrument {0} downloaded {1} candles", instrument, candles.Count);
            }
            catch (WebException ex)
            {
                Logger.Error("Connection error during request Oanda", ex);
                throw;
            }
            finally
            {
                m_OandaApiFactory.Release(apiClient);
            }
            return candles;
        }

        private CandleMidModel MapFromAPi(CandleMid candleMid, string instrument, string TimeFrame)
        {
            return new CandleMidModel
            {
                Time = candleMid.Time,
                Open = candleMid.OpenMid,
                Close = candleMid.CloseMid,
                High = candleMid.HighMid,
                Low = candleMid.LowMid,
                Instrument = instrument,
                TimeFrame = TimeFrame
            };
        }

        private Candle Map(CandleMid candle)
        {
            return new Candle
            {
                Time = candle.Time,
                Open = candle.OpenMid,
                Close = candle.CloseMid,
                High = candle.HighMid,
                Low = candle.LowMid,
                Complete = candle.Complete
            };
        }

        private TimeSpan getSpan(string TimeFrame)
        {
            switch (TimeFrame)
            {
                case "S5": return new TimeSpan(0, 0, 0, 5);

                default:
                    throw new ArgumentException(string.Format("Span spec for timeframe {0} not defined", TimeFrame));
            }
        }

        private int getSecondsInTf(string timeFrame)
        {
            return (int)getSpan(timeFrame).TotalSeconds;
        }

        private void debugFormat(string format, params object[] args)
        {
            if (m_Configuration.DebugLogs)
            {
                Logger.DebugFormat(format, args);
            }
        }
    }
}