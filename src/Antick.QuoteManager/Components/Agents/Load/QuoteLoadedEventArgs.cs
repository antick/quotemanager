﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.IncApp.QuoteManager.Components.Agents.Load
{
    public class QuoteLoadedEventArgs : EventArgs
    {
        public string Instrument { get; set; }
    }
}
