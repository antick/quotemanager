﻿using System;
using Antick.Cqrs.Commands;
using Antick.Infractructure.Components.Agent;
using Antick.QuoteManager.Cqs.Commands.Context;
using Castle.Core.Logging;

namespace Antick.QuoteManager.Components.Agents
{
   
    /// <summary>
    /// Агент по сохранению запакованных в ZIP файликах баров по каждому символу за каждый день на диск
    /// </summary>
    /// <remarks>
    /// В начале запускается со всеми инструментами. Активация каждую минуту + по событию из DailyCacheAgent (по каждому инструменту)
    /// </remarks>
    public class HistoryCacheAgent : BackAgent
    {
        protected override TimeSpan ErrorDelayTime => new TimeSpan(0, 1, 0);

        private readonly ICommandBuilder m_CommandBuilder;

        public HistoryCacheAgent(ILogger logger, ICommandBuilder commandBuilder) :base(logger)
        {
            m_CommandBuilder = commandBuilder;
        }

        protected override TimeSpan? Execute()
        {
            m_CommandBuilder.Execute(new HistoryLoaderIterationCommandContext {UseAllInstruments = true});
            return TimeSpan.FromMinutes(1);
        }
    }
}