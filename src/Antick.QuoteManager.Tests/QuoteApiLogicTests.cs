﻿using NUnit.Framework;

namespace Antick.QuoteManager.Tests
{
    [TestFixture]
    public class QuoteApiLogicTests
    {
        [Test]
        public void GetFirstQuoteShouldBeOne()
        {
            //// Setup mock
            //var quoteDbService = new Mock<IQuoteDbService>();
            //quoteDbService.Setup(x => x.GetFirstCandle(It.IsAny<string>()))
            //    .Returns(Task.FromResult(new CandleMidModel()));

            //// Setup core components
            //var apiLogic = new QuoteApiLogic(quoteDbService.Object, null, null);

            //// Do
            //var result = apiLogic.GetFirstCandle("EUR_USD").GetSynchronousResult();

            //// Verify
            //Assert.IsNotNull(result);
        }

        [Test]
        public void GetFirstCandleShouldBeNull()
        {
            //// Setup mock
            //var quoteDbService = new Mock<IQuoteDbService>();
            //quoteDbService.Setup(x => x.GetFirstCandle(It.IsAny<string>()))
            //    .Returns(Task.FromResult((CandleMidModel) null));

            //// Setup core components
            //var apiLogic = new QuoteApiLogic(quoteDbService.Object, null, null);

            //// Do
            //var result = apiLogic.GetFirstCandle("EUR_USD").GetSynchronousResult();

            //// Verify
            //Assert.IsNull(result);
        }
    }
}
