﻿using System;
using System.Collections.Generic;
using Antick.Cqrs.Queries;
using Antick.QuoteManager.Components.MemoryCache;
using Antick.QuoteManager.Domain;
using Moq;
using NUnit.Framework;

namespace Antick.QuoteManager.Tests.Components.MemoryCache
{
    public class HistoryLoaderLastTimeMemoryCacheTests
    {
        
        private IQueryBuilder mockQueryBuilder()
        {
            var iqueryFor = new Mock<IQueryFor<List<HistoryLoaderStatus>>>();
            iqueryFor.Setup(x => x.With(It.IsAny<ICriterion>()))
                .Returns(new List<HistoryLoaderStatus>
                {
                    new HistoryLoaderStatus(0, "EUR_USD", new DateTime(2017, 5, 21, 0, 0, 0, DateTimeKind.Utc)),
                    new HistoryLoaderStatus(0, "USD_CAD", new DateTime(2017, 5, 20, 0, 0, 0, DateTimeKind.Utc))
                });

            var mock = new Mock<IQueryBuilder>();
            mock.Setup(x => x.For<List<HistoryLoaderStatus>>()).Returns(iqueryFor.Object);

            return mock.Object;
        }

        /// <summary>
        /// Должны достать эталонное время указанной пары
        /// </summary>
        [Test]
        public void ShouldGetLastTime()
        {
            var memoryCache = new HistoryLoaderLastTimeMemoryCache(mockQueryBuilder());

            memoryCache.Init();

            var time = memoryCache.Get("EUR_USD");
            Assert.IsTrue(time.HasValue && time.Value == new DateTime(2017, 5, 21, 0, 0, 0, DateTimeKind.Utc));
        }

        /// <summary>
        /// Должно обновляться время 
        /// </summary>
        [Test]
        public void ShouldUpdateLastTime()
        {
            var memoryCache = new HistoryLoaderLastTimeMemoryCache(mockQueryBuilder());
            memoryCache.Init();

            memoryCache.Update("EUR_USD", new DateTime(2017, 5, 22, 0, 0, 0, DateTimeKind.Utc));

            var time = memoryCache.Get("EUR_USD");
            Assert.IsTrue(time.HasValue && time.Value == new DateTime(2017, 5, 22, 0, 0, 0, DateTimeKind.Utc));
        }
    }
}
