﻿using System;
using System.Collections.Generic;
using Antick.OandaApi;
using Antick.OandaApi.Models;
using Moq;
using NUnit.Framework;

namespace Antick.QuoteManager.Tests.Queries
{
    [TestFixture(Category = "DownloadQuotesQuery")]
    public class DownloadQuotesQueryTests
    {
        private IOandaClient mockOandaClient(List<CandleMid> response)
        {
            var mock = new Mock<IOandaClient>();
            mock.Setup(x => x.GetCandlesMid(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>(),
                    It.IsAny<int>()))
                .Returns(response);
            return mock.Object;
        }

        ///// <summary>
        ///// Всегда должны забирать только завершенные бары
        ///// </summary>
        //[Test]
        //public void ShouldTakeOnlyCompletedCandles()
        //{
        //    var input = new DownloadCandles
        //    {
        //        DefaultStartTime = new DateTime(2016, 1, 1),
        //        Instrument = "EUR_USD",
        //        StartTime = null,
        //        TimeFrame = "S5"
        //    };

        //    var apiResponse = new List<CandleMid>
        //    {
        //        new CandleMid {Complete = false, Time = new DateTime(2016, 1, 1, 0, 0, 5, DateTimeKind.Utc)}
        //    };

        //    var query = new DownloadCandlesQuery(mockOandaClient(apiResponse), new ConsoleLogger());
        //    var queryResponse = query.Ask(input);

        //    Assert.IsTrue(queryResponse.Count == 0);
        //}

        ///// <summary>
        ///// При первом запуске должны взять все заверешные бары, в том числе и самый первый
        ///// </summary>
        //[Test]
        //public void OnStartShouldGetAllCompletedCandles()
        //{
        //    var input = new DownloadCandles
        //    {
        //        DefaultStartTime = new DateTime(2016, 1, 1),
        //        Instrument = "EUR_USD",
        //        StartTime = null,
        //        TimeFrame = "S5"
        //    };

        //    var apiResponse = new List<CandleMid>
        //    {
        //        new CandleMid {Complete = true, Time = new DateTime(2016, 1, 1, 0, 0, 0, DateTimeKind.Utc)}
        //    };

        //    var query = new DownloadCandlesQuery(mockOandaClient(apiResponse), new ConsoleLogger());
        //    var queryResponse = query.Ask(input);

        //    Assert.IsTrue(queryResponse.Count == 1);
        //}

        ///// <summary>
        ///// В конечной выборке мы должны не брать первый бар старта
        ///// </summary>
        //[Test]
        //public void ShouldNotTakeFirstCandle()
        //{
        //    var input = new DownloadCandles
        //    {
        //        DefaultStartTime = new DateTime(2016, 1, 1),
        //        Instrument = "EUR_USD",
        //        StartTime = new DateTime(2016, 1, 1, 0, 0, 5, DateTimeKind.Utc),
        //        TimeFrame = "S5"
        //    };

        //    var apiResponse = new List<CandleMid>
        //    {
        //        new CandleMid {Complete = true, Time = new DateTime(2016, 1, 1, 0, 0, 5, DateTimeKind.Utc)},
        //        new CandleMid {Complete = true, Time = new DateTime(2016, 1, 1, 0, 0, 10, DateTimeKind.Utc)},
        //        new CandleMid {Complete = false, Time = new DateTime(2016, 1, 1, 0, 0, 15, DateTimeKind.Utc)}
        //    };

        //    var query = new DownloadCandlesQuery(mockOandaClient(apiResponse), new ConsoleLogger());
        //    var queryResponse = query.Ask(input);

        //    Assert.IsTrue(queryResponse.Count(p => p.Time == input.StartTime) == 0 && queryResponse.Count == 1);
        //}
    }
}