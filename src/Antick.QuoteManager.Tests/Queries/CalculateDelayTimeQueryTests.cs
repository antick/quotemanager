﻿using System;
using Antick.QuoteManager.Cqs.Queries;
using Antick.QuoteManager.Cqs.Queries.Criterias;
using Castle.Core.Logging;
using NUnit.Framework;

namespace Antick.QuoteManager.Tests.Queries
{
    [TestFixture(Category = "CalculateDelayTimeQuery")]
    public class CalculateDelayTimeQueryTests
    {
        /// <summary>
        /// Режим "GrubMode" должен активировать ожидание в 0 секунд
        /// </summary>
        [Test]
        public void GrubModeSpanShouldBeZero()
        {
            var query = new CalculateDelayTimeQuery(Mocks.Clock(DateTime.UtcNow), new ConsoleLogger());

            var timespan = query.Ask(new CalculateDelayTime {AnyDownloaded = true, IsGrubMode = true, TimeFrame = "S5"});

            Assert.IsTrue(timespan == new TimeSpan(0,0,0));
        }

        /// <summary>
        /// В выходные и когда не было выкачано ни одного данного, должно быть ожидание в 1 минуту
        /// </summary>
        [Test]
        public void InHolidayNoDataDelayShouldBeOneMinute()
        {
            var saturday = new DateTime(2017, 5, 13, 0, 0, 0, 0, DateTimeKind.Utc);

            var query = new CalculateDelayTimeQuery(Mocks.Clock(saturday), new ConsoleLogger());

            var timespan = query.Ask(new CalculateDelayTime { AnyDownloaded = false, IsGrubMode = false, TimeFrame = "S5" });

            Assert.IsTrue(timespan == new TimeSpan(0, 1, 0));
        }


        /// <summary>
        /// В выходные и когда было выкачано немного данных, должно быть ожидание меньше минуты, для S5 в районе 5 сек
        /// </summary>
        [Test]
        public void InHolidayAnyDataDelayShouldBeLessOneMinute()
        {
            var saturday = new DateTime(2017, 5, 13, 0, 0, 0, 0, DateTimeKind.Utc);

            var query = new CalculateDelayTimeQuery(Mocks.Clock(saturday), new ConsoleLogger());

            var timespan = query.Ask(new CalculateDelayTime { AnyDownloaded = true, IsGrubMode = false, TimeFrame = "S5" });

            Assert.IsTrue(timespan < new TimeSpan(0, 1, 0));
        }
    }
}
