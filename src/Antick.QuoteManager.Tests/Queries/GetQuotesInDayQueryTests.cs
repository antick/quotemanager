﻿using System;
using System.Collections.Generic;
using Antick.Contracts;
using Antick.Cqrs.Queries;
using Moq;
using NUnit.Framework;

namespace Antick.QuoteManager.Tests.Queries
{
    [TestFixture(Category = "GetQuotesInDayQuery")]
    public class GetQuotesInDayQueryTests
    {
        private IQueryBuilder mockQueryBuilder()
        {
            var iqueryFor = new Mock<IQueryFor<List<Quote>>>();
            iqueryFor.Setup(x => x.With(It.IsAny<ICriterion>()))
                .Returns(new List<Quote>
                {
                    new Quote{Time = new DateTime(2017,5,18,0,0,0,DateTimeKind.Utc)}
                });

            var mock = new Mock<IQueryBuilder>();
            mock.Setup(x => x.For<List<Quote>>()).Returns(iqueryFor.Object);

            return mock.Object;
        }

        // Сейчас тесты не нужны
    }
}
