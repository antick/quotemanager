﻿using System;
using Antick.Infractructure.Components;
using Antick.Infractructure.Components.Misc;
using Moq;

namespace Antick.QuoteManager.Tests
{
    public class Mocks
    {
        public static IClock Clock(DateTime time)
        {
            var local = DateTime.SpecifyKind(time, DateTimeKind.Utc).ToLocalTime();

            var mock = new Mock<IClock>();
            mock.Setup(x => x.UtcNow()).Returns(time);
            mock.Setup(x => x.Now()).Returns(local);

            return mock.Object;
        }
    }
}
